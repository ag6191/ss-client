package org.ss.game.object;

import org.joml.Vector3f;
import org.ss.engine.graph.GraphObject;
import org.ss.engine.graph.Material;
import org.ss.engine.graph.Mesh;
import org.ss.engine.loader.OBJLoader;
import org.ss.engine.graph.Texture;

// Class for a skybox object that surrounds the game scene with a set texture
public class SkyboxObject extends GraphObject {

    // Constructor for the default SkyboxObject class
    public SkyboxObject(String objModel, String textureFile) throws Exception{
        super(OBJLoader.loadMesh(objModel),500f,new Vector3f(0,0,0));

        // Sets up skybox mesh/texture
        Texture skyBoxTexture = new Texture(textureFile);
        Mesh skyBoxMesh = OBJLoader.loadMesh(objModel);
        skyBoxMesh.setMaterial(new Material (skyBoxTexture, 0.0f));

        this.setMesh(skyBoxMesh);
        this.setPosition(0,0,0);
        this.setRotation(0,0,0);
    }
}
