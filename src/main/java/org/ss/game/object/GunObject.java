package org.ss.game.object;

import org.joml.Vector3f;
import org.joml.Vector4f;
import org.ss.engine.Timer;
import org.ss.engine.graph.Material;
import org.ss.engine.graph.Mesh;
import org.ss.engine.loader.OBJLoader;

// Class for a gun object that fires projectiles
public class GunObject {

    private final float damage;

    private final float range;

    private final float velocity;

    private Mesh projectileMesh;

    private float projectileScale;

    private final Timer timer;

    private double lastShotTime;

    // Constructor for the GunObject class
    public GunObject(float damage, float range, float velocity) {
        this.damage = damage;
        this.range = range;
        this.velocity = velocity;
        this.timer = new Timer();
        this.lastShotTime = timer.getTime();

        // Create and set the projectile mesh attribute
        try {
            this.projectileMesh = OBJLoader.loadMesh("/models/bullet01.obj");
            Material projectileMaterial = new Material(new Vector4f(1.0f, 0.2f, 0.2f, 1.0f), 1.0f);
            this.projectileMesh.setMaterial(projectileMaterial);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.projectileScale = 1.0f;
    }

    // Constructor for the GunObject class with the option to not load a mesh
    public GunObject(float damage, float range, float velocity, boolean noMesh) {
        this.damage = damage;
        this.range = range;
        this.velocity = velocity;
        this.timer = new Timer();
        this.lastShotTime = timer.getTime();
        if (!noMesh) {
            try {
                this.projectileMesh = OBJLoader.loadMesh("/models/bullet01.obj");
                Material projectileMaterial = new Material(new Vector4f(1.0f, 0.2f, 0.2f, 1.0f), 1.0f);
                this.projectileMesh.setMaterial(projectileMaterial);
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.projectileScale = 1.0f;
        }
    }

    // Returns a new ProjectileObject from a given position with a specific range and velocity
    public ProjectileObject fireGun(Vector3f parentGunPosition, Vector3f parentVelocity, Vector3f parentRotation) {
        this.lastShotTime = timer.getTime();
        ProjectileObject projectileObject = new ProjectileObject(projectileMesh, projectileScale, new Vector3f(parentGunPosition), range, damage);
        projectileObject.setVelocity(new Vector3f().set(parentVelocity).mul(velocity));
        projectileObject.setRotation(parentRotation.x, parentRotation.y, parentRotation.z);
        return projectileObject;
    }
}
