package org.ss.game.object;

import org.joml.Vector3f;
import org.ss.engine.graph.Mesh;

// Class for a ship object that has a gun object attached to it
public class ShipObject extends GameObject {

    private GunObject gunObject = null;

    // Constructor for the ShipObject class
    public ShipObject(Mesh mesh, float scale, Vector3f position) throws Exception {
        super(mesh, scale, position);
        initGunObject();
    }

    // Initializes a new GunObject object as an attribute of this class with default parameters
    public void initGunObject() throws Exception {
        this.gunObject = new GunObject(30.0f, 100.0f, 5.0f);
    }

    // Getter for the gunObject attribute
    public GunObject getGunObject() {
        return gunObject;
    }
}
