package org.ss.game.object;

import org.joml.Vector3f;
import org.ss.engine.graph.Mesh;
import org.ss.engine.phys.ColliderType;
import org.ss.engine.phys.PhysicsObject;

// Class for a projectile object that expires after a certain distance
public class ProjectileObject extends PhysicsObject {

    private final Vector3f originPosition;

    private final float range;

    private final float damage;

    // Constructor for the ProjectileObjectClass
    public ProjectileObject(Mesh mesh, float scale, Vector3f position, float range, float damage) {
        super(mesh, scale, position, ColliderType.BOUNDING_SPHERE);
        originPosition = new Vector3f(position);
        this.range = range;
        this.damage = damage;
    }

    // Returns true if the length of the vector from the current position to the original position is greater than the range
    public boolean outOfRange() {
        return new Vector3f(super.getPosition()).sub(originPosition).length() > range;
    }

    // Getter for the originalPosition attribute
    public Vector3f getOriginPosition() {
        return originPosition;
    }

    // Getter for the range attribute
    public float getRange() {
        return range;
    }

    // Getter for the damage attribute
    public float getDamage() {
        return damage;
    }
}
