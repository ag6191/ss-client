package org.ss.game.object;

import org.joml.Vector3f;
import org.ss.engine.Timer;
import org.ss.engine.graph.Mesh;
import org.ss.engine.phys.ColliderType;
import org.ss.engine.phys.PhysicsObject;

// Class for a particle object that expires after a certain duration
public class ParticleObject extends PhysicsObject {

    private final float particleDurationTime;

    private final double particleCreationTime;

    // Constructor for the ParticleObject class
    public ParticleObject(Mesh mesh, float scale, Vector3f position, float particleDuration, double particleCreationTime) {
        super(mesh, scale, position, ColliderType.NONE);
        this.particleDurationTime = particleDuration;
        this.particleCreationTime = particleCreationTime;
    }

    // Returns true if the current time is greater than the sum of the particle creation time and duration
    public boolean canDeleteParticle() {
        return (particleCreationTime + particleDurationTime) < Timer.getTime();
    }
}
