package org.ss.game.object;

import org.joml.Vector3f;
import org.ss.engine.Utils;
import org.ss.engine.graph.Mesh;
import org.ss.engine.phys.ColliderType;
import org.ss.engine.phys.PhysicsObject;
import org.ss.engine.Timer;

// Class for a general game object
public class GameObject extends PhysicsObject {

    private float health;

    private Mesh explosionParticleMesh = null;

    private float explosionParticleScale;

    private int countExplosionParticles = 0;

    // Constructor for the GameObject class with a specific collider type and health value
    public GameObject(Mesh mesh, float scale, Vector3f position, ColliderType colliderType, float health) {
        super(mesh, scale, position, colliderType);
        this.health = health;
    }

    // Constructor for the GameObject class that uses a BoundingSphere collider and health of 100.0f by default
    public GameObject(Mesh mesh, float scale, Vector3f position) {
        this(mesh, scale, position, ColliderType.BOUNDING_SPHERE, 100.0f);
    }

    // Getter for the health attribute
    public float getHealth() {
        return health;
    }

    // Setter for the health attribute
    public void setHealth(float health) {
        this.health = health;
    }

    // Reduces health by given damage amount
    public void reduceHealth(float damage) {
        this.health -= damage;
    }

    // Sets the particle mesh, count, and scale attributes with arguments
    public void setExplosionParticleMesh(Mesh explosionParticleMesh, int countExplosionParticles) {
        this.explosionParticleMesh = explosionParticleMesh;
        this.explosionParticleScale = super.getScale()/2;
        this.countExplosionParticles = countExplosionParticles;
    }

    // Returns a newly created collection of ParticleObjects from associated particle object attributes
    public ParticleObject[] getExplosionParticleArr() {
        ParticleObject[] explosionParticleArr = new ParticleObject[countExplosionParticles];
        if (explosionParticleMesh != null) {
            for (int i = 0; i < countExplosionParticles; i++) {
                explosionParticleArr[i] = new ParticleObject(explosionParticleMesh, explosionParticleScale, super.getCollider().getPosition(), 3.0f, Timer.getTime());
                explosionParticleArr[i].setVelocity(
                        Utils.randFloat(-5.0f, 5.0f),
                        Utils.randFloat(-5.0f, 5.0f),
                        Utils.randFloat(-5.0f, 5.0f));
                explosionParticleArr[i].setRotation(
                        Utils.randFloat(-3.0f, 3.0f),
                        Utils.randFloat(-3.0f, 3.0f),
                        Utils.randFloat(-3.0f, 3.0f));
            }
        }
        return explosionParticleArr;
    }
}
