package org.ss.game.logic;

import org.joml.Vector2f;
import org.joml.Vector3f;
import java.lang.Math;
import static org.lwjgl.glfw.GLFW.*;
import org.joml.Vector4f;
import org.ss.engine.*;
import org.ss.engine.graph.*;
import org.ss.engine.loader.GameObjectLoader;
import org.ss.engine.loader.OBJLoader;
import org.ss.game.object.GameObject;
import org.ss.game.object.ProjectileObject;
import org.ss.game.object.ShipObject;
import org.ss.engine.Renderer;
import org.ss.game.object.SkyboxObject;

// Class for the current game's logic
public class SinglePlayerGame implements IGameLogic {

    private final Vector3f cameraInc;

    private final Renderer renderer;

    private final Camera camera;

    private ShipObject playerShip;

    private Scene scene;

    private String state;

    // Constructor for the SinglePlayerGame logic class
    public SinglePlayerGame() {
        renderer = new Renderer();
        camera = new Camera();
        scene = new Scene();
        cameraInc = new Vector3f(0.0f, 0.0f, 0.0f);
    }

    // Initializer for this game logic and everything that goes into the scene
    @Override
    public void init(Window window) throws Exception {
        renderer.init(window);
        state = "GAME";

        // Load meshes
        Mesh cubeMesh = OBJLoader.loadMesh("/models/cube.obj");
        Mesh particle0Mesh = OBJLoader.loadMesh("/models/shipParticle0.obj");
        Mesh playerShipMesh = OBJLoader.loadMesh("/models/Ship002.obj");
        Mesh asteroidMesh = OBJLoader.loadMesh("/models/Asteroid01.obj");

        // Create textures/materials for game objects
        float reflectance = 1f;
        Texture cubeTexture = new Texture("textures/grassblock.png");
        Material cubeMaterial = new Material(cubeTexture, reflectance);
        Material particle0Material = new Material(new Vector4f(1.0f, 1.0f, 1.0f, 1.0f), reflectance);
        Material playerMaterial = new Material(new Vector4f(0.9f, 0.9f, 0.9f, 1.0f), reflectance);
        Material asteroidMaterial = new Material(new Vector4f(0.30f, 0.30f, 0.30f, 1.0f), reflectance);

        // Set materials
        cubeMesh.setMaterial(cubeMaterial);
        particle0Mesh.setMaterial(particle0Material);
        playerShipMesh.setMaterial(playerMaterial);
        asteroidMesh.setMaterial(asteroidMaterial);

        //Create Skybox
        SkyboxObject skyBox = new SkyboxObject("/models/skybox.obj", "textures/skybox.png");
        scene.setSkyBox(skyBox);

        // Create player ship and add it to the physics engine
        float shipScale = 0.5f;
        playerShip = GameObjectLoader.spawnShipObject(playerShipMesh, shipScale, new Vector3f(0, 0, -3));
        playerShip.setRotation(0,0,0);
        scene.physicsEngine.addGameObject(playerShip);

        // Set camera slightly above the player ship
        camera.setPosition(camera.getPosition().x, camera.getPosition().y + Utils.CAMERA_Y_POS_OFFSET, camera.getPosition().z);

        // Create asteroids
        int numAsteroids = 2500;
        float minScale = 0.5f;
        float maxScale = 5.00f;
        int spawnAreaDim = 200;
        GameObject[] asteroidGameObjectArr = new GameObject[numAsteroids];
        for (int i = 0; i < numAsteroids; i++) {
            float asteroidScale = Utils.randFloat(minScale, maxScale);
            asteroidGameObjectArr[i] = GameObjectLoader.spawnGameObjectInArea(asteroidMesh, asteroidScale, spawnAreaDim, spawnAreaDim, spawnAreaDim);
            asteroidGameObjectArr[i].setExplosionParticleMesh(particle0Mesh, 45);
            GameObjectLoader.randomizeGameObjectVelocity(asteroidGameObjectArr[i], -1.0f, 1.0f);
        }

        // Add all game objects to the physics engine
        scene.physicsEngine.addGameObjectArr(asteroidGameObjectArr);
    }

    // Method that updates the game logic attributes with respect to the window, keyboard, and mouse input
    @Override
    public void input(Window window, MouseInput mouseInput) {
        float x,y,z;

        // Set camera increment vector and player ship's velocity
        cameraInc.set(playerShip.getVelocity().x, 0, playerShip.getVelocity().z);
        playerShip.setVelocity(playerShip.getVelocity().x, 0, playerShip.getVelocity().z);

        // Adjust the ship's forward movement
        if (window.isKeyPressed(GLFW_KEY_W) && state.equals("GAME")) { // Accelerate
            if (playerShip.getVelocity().z > -15.0f) {
                x = playerShip.getVelocity().x;
                y = playerShip.getVelocity().y;
                z = playerShip.getVelocity().z - 0.2f;
                playerShip.setVelocity(x, y, z);
            }
            cameraInc.set(playerShip.getVelocity());

        } else if (window.isKeyPressed(GLFW_KEY_S) && state.equals("GAME")) { //Decelerate
            if (playerShip.getVelocity().z < -2.0f) {
                x = playerShip.getVelocity().x;
                y = playerShip.getVelocity().y;
                z = playerShip.getVelocity().z + 0.9f;
                playerShip.setVelocity(x, y, z);
            }
            cameraInc.set(playerShip.getVelocity());
        }

        // Adjust the ship's rotation
        if (window.isKeyPressed(GLFW_KEY_A) && state.equals("GAME")) { // Bank left
            turnShip(playerShip, camera);
            camera.radians_h +=.02;
            camera.moveRotation(0,-1.15f,0);
            if (playerShip.getRotation().z > 0)
                playerShip.moveRotation(0,-1.15f,-.8f);
            else
                playerShip.moveRotation(0,-1.15f,-.4f);

        } else if (window.isKeyPressed(GLFW_KEY_D) && state.equals("GAME")) { // Bank right
            turnShip(playerShip, camera);
            camera.radians_h -=.02;
            camera.moveRotation(0,1.15f,0);
            if (playerShip.getRotation().z < 0)
                playerShip.moveRotation(0,1.15f,.8f);
            else
                playerShip.moveRotation(0,1.15f,.4f);

        } else if (playerShip.getRotation().z < 0){ // Return from left bank
            playerShip.moveRotation(0,0,.6f);
        } else if (playerShip.getRotation().z > 0){ // Return from right bank
            playerShip.moveRotation(0,0,-.6f);
        }

        // Adjust the ship's up and down movement
        if (window.isKeyPressed(GLFW_KEY_Z) && state.equals("GAME")) { // Move ship up
            cameraInc.set(cameraInc.x, -4f, cameraInc.z);
            playerShip.setVelocity(cameraInc);

        } else if (window.isKeyPressed(GLFW_KEY_X) && state.equals("GAME")) { // Move ship down
            cameraInc.set(cameraInc.x, 4f, cameraInc.z);
            playerShip.setVelocity(cameraInc);
        }
    }

    // Method to turn the ship and camera
    private static void turnShip(ShipObject playerShip, Camera camera) {
        float x, y, z;
        x = (float) (playerShip.getPosition().x + 3 * Math.cos(camera.radians_h-(Math.PI/2)) * Math.cos(camera.radians_v));
        y = (float) (playerShip.getPosition().y + 3 * Math.sin(camera.radians_v));
        z = (float) (playerShip.getPosition().z - 3 * Math.sin(camera.radians_h-(Math.PI/2)) * Math.cos(camera.radians_v));
        camera.setPosition(x, y + Utils.CAMERA_Y_POS_OFFSET, z);
    }

    // Updates the items in the scene with respect to inputs and game logic
    @Override
    public void update(float interval, MouseInput mouseInput) {
        // Move/rotate camera
        if (mouseInput.isRightButtonPressed()) {
            Vector2f rotVec = mouseInput.getDisplVec();
            camera.moveRotation(rotVec.x * Utils.MOUSE_SENSITIVITY, rotVec.y * Utils.MOUSE_SENSITIVITY, 0);
        }
        camera.movePosition(cameraInc.x * Utils.CAMERA_POS_STEP, cameraInc.y * Utils.CAMERA_POS_STEP, cameraInc.z * Utils.CAMERA_POS_STEP);

        if (playerShip.getGunObject() != null && mouseInput.isLeftButtonPressed()) {
            ProjectileObject projectile = playerShip.getGunObject().fireGun(playerShip.getPosition(), playerShip.getVelocity(), playerShip.getRotation());
            scene.physicsEngine.addProjectileObject(projectile);
        }
        // Move everything else
        scene.physicsEngine.updateObjects();
        scene.physicsEngine.detectCollisions();
    }

    // Call the render method to render game scene
    @Override
    public void render(Window window) {
        renderer.render(window, camera, scene);
    }

    // Cleanup and delete everything in the game scene
    @Override
    public void cleanup() {
        renderer.cleanup();
        scene.physicsEngine.cleanup();
    }
}
