package org.ss.game;

import org.ss.engine.GameEngine;
import org.ss.engine.IGameLogic;
import org.ss.game.logic.SinglePlayerGame;

public class Main {

    // Main method that starts the program
    public static void main(String[] args) {
        try {
            // Window parameters
            boolean vSync = true;
            int WIDTH = 1200;
            int HEIGHT = 900;

            // Initialize the game logic and engine
            IGameLogic gameLogic = new SinglePlayerGame();
            GameEngine gameEng = new GameEngine("GAME", WIDTH, HEIGHT, vSync, gameLogic);

            // Start the game
            gameEng.start();
        } catch (Exception excp) {
            excp.getStackTrace();
            System.exit(-1);
        }
    }
}
