package org.ss.engine;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import static org.lwjgl.opengl.GL11.*;
import org.ss.engine.graph.*;
import org.ss.engine.graph.light.DirectionalLight;
import org.ss.engine.graph.light.PointLight;
import org.ss.engine.graph.light.SceneLight;
import org.ss.engine.graph.light.SpotLight;
import org.ss.engine.phys.PhysicsObject;
import org.ss.game.object.SkyboxObject;

// Class that handles rendering all items in the game scene
public class Renderer {

    private static final float FOV = (float) Math.toRadians(60.0f);

    private static final float Z_NEAR = 0.01f;

    private static final float Z_FAR = 1000.0f;

    private static final int MAX_POINT_LIGHTS = 5;

    private static final int MAX_SPOT_LIGHTS = 5;

    private final Transformation transformation;

    private ShaderProgram sceneShaderProgram;

    private ShaderProgram skyboxShaderProgram;

    private final float specularPower;

    // Constructor for the Renderer class
    public Renderer() {
        transformation = new Transformation();
        specularPower = 10f;
    }

    // Initializes the renderer and sets up the proper shader programs
    public void init(Window window) throws Exception {
        setupSceneShader();
        setupSkyBoxShader();
    }

    // Sets up the SkyBox object shader program
    private void setupSkyBoxShader() throws Exception {
        // Create shader
        skyboxShaderProgram = new ShaderProgram();
        skyboxShaderProgram.createVertexShader(Utils.loadResource("/shaders/skyboxVertex.vs"));
        skyboxShaderProgram.createFragmentShader(Utils.loadResource("/shaders/skyboxFragment.fs"));
        skyboxShaderProgram.link();

        // Create uniforms for projection matrix
        skyboxShaderProgram.createUniform("projectionMatrix");
        skyboxShaderProgram.createUniform("modelViewMatrix");
        skyboxShaderProgram.createUniform("texture_sampler");
        skyboxShaderProgram.createUniform("ambientLight");
    }

    // Sets up the game scene Shader
    private void setupSceneShader() throws Exception {
        // Create shader
        sceneShaderProgram = new ShaderProgram();
        sceneShaderProgram.createVertexShader(Utils.loadResource("/shaders/sceneVertex.vs"));
        sceneShaderProgram.createFragmentShader(Utils.loadResource("/shaders/sceneFragment.fs"));
        sceneShaderProgram.link();

        // Create uniforms for modelView and projection matrices and texture
        sceneShaderProgram.createUniform("projectionMatrix");
        sceneShaderProgram.createUniform("modelViewMatrix");
        sceneShaderProgram.createUniform("texture_sampler");
        // Create uniform for material
        sceneShaderProgram.createMaterialUniform("material");
        // Create lighting related uniforms
        sceneShaderProgram.createUniform("specularPower");
        sceneShaderProgram.createUniform("ambientLight");
        sceneShaderProgram.createPointLightListUniform("pointLights", MAX_POINT_LIGHTS);
        sceneShaderProgram.createSpotLightListUniform("spotLights", MAX_SPOT_LIGHTS);
        sceneShaderProgram.createDirectionalLightUniform("directionalLight");
    }

    // Clears the opengl buffers to be refilled in the next frame
    public void clear() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    // Clears the opengl buffer and refills them with a newly rendered frame
    public void render(Window window, Camera camera, Scene scene) {
        clear();

        renderSkybox(window, camera, scene);

        renderScene(window, camera, scene);
    }

    // Renders all items in the game scene with respect to the camera
    private void renderScene(Window window, Camera camera, Scene scene) {
        if (window.isResized()) {
            glViewport(0, 0, window.getWidth(), window.getHeight());
            window.setResized(false);
        }
        SceneLight sceneLight = scene.getSceneLight();
        sceneShaderProgram.bind();

        // Update the projection matrix
        Matrix4f projectionMatrix = transformation.getProjectionMatrix(FOV, window.getWidth(), window.getHeight(), Z_NEAR, Z_FAR);
        sceneShaderProgram.setUniform("projectionMatrix", projectionMatrix);

        // Update view Matrix
        Matrix4f viewMatrix = transformation.getViewMatrix(camera);

        // Update Light Uniforms
        renderLights(viewMatrix, sceneLight);

        sceneShaderProgram.setUniform("texture_sampler", 0);

        // Render each gameItem
        renderItems(scene.getGameObjectArr(), viewMatrix, transformation);

        // Render each particle object
        renderItems(scene.getParticleObjectArr(), viewMatrix, transformation);

        // Render each projectile object
        renderItems(scene.getProjectileObjectArr(), viewMatrix, transformation);

        sceneShaderProgram.unbind();
    }

    // Renders a collection of Physics objects
    private void renderItems(PhysicsObject[] gameObjectArr, Matrix4f viewMatrix, Transformation transformation) {
        for (PhysicsObject physicsObject : gameObjectArr) {
            if (physicsObject != null){
                Mesh mesh = physicsObject.getMesh();

                // Set modelView matrix for this item
                Matrix4f modelViewMatrix = transformation.getModelViewMatrix(physicsObject, viewMatrix);
                sceneShaderProgram.setUniform("modelViewMatrix", modelViewMatrix);

                // Render the mesh
                sceneShaderProgram.setUniform("material", mesh.getMaterial());
                mesh.render();
            }
        }
    }

    // Renders the scene's SkyBox with respect to the camera
    private void renderSkybox(Window window, Camera camera, Scene scene) {
        skyboxShaderProgram.bind();

        skyboxShaderProgram.setUniform("texture_sampler", 0);

        // Update projection Matrix
        Matrix4f projectionMatrix = transformation.getProjectionMatrix(FOV, window.getWidth(), window.getHeight(), Z_NEAR, Z_FAR);
        skyboxShaderProgram.setUniform("projectionMatrix", projectionMatrix);
        SkyboxObject skyBox = scene.getSkyBox();
        Matrix4f viewMatrix = transformation.getViewMatrix(camera);
        viewMatrix.m30(0);
        viewMatrix.m31(0);
        viewMatrix.m32(0);
        Matrix4f modelViewMatrix = transformation.getModelViewMatrix(skyBox, viewMatrix);
        skyboxShaderProgram.setUniform("modelViewMatrix", modelViewMatrix);
        skyboxShaderProgram.setUniform("ambientLight", scene.getSceneLight().getAmbientLight());

        scene.getSkyBox().getMesh().render();

        skyboxShaderProgram.unbind();
    }

    // Renders all of the lights in the scene
    private void renderLights(Matrix4f viewMatrix, SceneLight sceneLight) {

        sceneShaderProgram.setUniform("ambientLight", sceneLight.getAmbientLight());
        sceneShaderProgram.setUniform("specularPower", specularPower);

        // Process Point Lights
        int numLights = sceneLight.getPointLightList() != null ? sceneLight.getPointLightList().length : 0;
        for (int i = 0; i < numLights; i++) {
            // Get a copy of the point light object and transform its position to view coordinates
            PointLight currPointLight = new PointLight(sceneLight.getPointLightList()[i]);
            Vector3f lightPos = currPointLight.getPosition();
            Vector4f aux = new Vector4f(lightPos, 1);
            aux.mul(viewMatrix);
            lightPos.x = aux.x;
            lightPos.y = aux.y;
            lightPos.z = aux.z;
            sceneShaderProgram.setUniform("pointLights", currPointLight, i);
        }

        // Process Spot Lights
        numLights = sceneLight.getSpotLightList() != null ? sceneLight.getSpotLightList().length : 0;
        for (int i = 0; i < numLights; i++) {
            // Get a copy of the spot light object and transform its position and cone direction to view coordinates
            SpotLight currSpotLight = new SpotLight(sceneLight.getSpotLightList()[i]);
            Vector4f dir = new Vector4f(currSpotLight.getConeDirection(), 0);
            dir.mul(viewMatrix);
            currSpotLight.setConeDirection(new Vector3f(dir.x, dir.y, dir.z));
            Vector3f lightPos = currSpotLight.getPointLight().getPosition();

            Vector4f aux = new Vector4f(lightPos, 1);
            aux.mul(viewMatrix);
            lightPos.x = aux.x;
            lightPos.y = aux.y;
            lightPos.z = aux.z;

            sceneShaderProgram.setUniform("spotLights", currSpotLight, i);
        }

        // Get a copy of the directional light object and transform its position to view coordinates
        DirectionalLight currDirLight = new DirectionalLight(sceneLight.getDirectionalLight());
        Vector4f dir = new Vector4f(currDirLight.getDirection(), 0);
        dir.mul(viewMatrix);
        currDirLight.setDirection(new Vector3f(dir.x, dir.y, dir.z));
        sceneShaderProgram.setUniform("directionalLight", currDirLight);

    }

    // Commences cleanup of the ShaderProgram objects
    public void cleanup() {
        if (sceneShaderProgram != null) {
            sceneShaderProgram.cleanup();
        }

        if (skyboxShaderProgram != null) {
            skyboxShaderProgram.cleanup();
        }
    }
}
