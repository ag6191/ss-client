package org.ss.engine.graph;

import org.joml.Vector3f;

// Class for the player camera in the environment
public class Camera {

    private final Vector3f position;

    private final Vector3f rotation;

    //horizontal radians
    public double radians_h;

    //vertical radians
    public double radians_v;

    // Constructor for the Camera class
    public Camera() {
        position = new Vector3f();
        rotation = new Vector3f(0,0,1);
        radians_h = 0;
        radians_v = 0;
    }

    // Constructor for the Camera class with a given position and rotation
    public Camera(Vector3f position, Vector3f rotation) {
        this.position = position;
        this.rotation = rotation;
    }

    // Getter for the position vector attribute
    public Vector3f getPosition() {
        return position;
    }

    // Setter for the position vector attribute
    public void setPosition(double x, float y, double z) {
        position.x = (float) x;
        position.y = y;
        position.z = (float) z;
    }

    // Adjusts the position of the camera with respect to a given 3D offset and position/rotation attributes
    public void movePosition(float offsetX, float offsetY, float offsetZ) {
        if (offsetZ != 0) {
            position.x += (float) Math.sin(Math.toRadians(rotation.y)) * -1.0f * offsetZ;
            position.z += (float) Math.cos(Math.toRadians(rotation.y)) * offsetZ;
        }
        if (offsetX != 0) {
            position.x += (float) Math.sin(Math.toRadians(rotation.y - 90)) * -1.0 * offsetX;
            position.z += (float) Math.cos(Math.toRadians(rotation.y - 90)) * offsetX;
        }
        position.y += offsetY;
    }

    // Getter for the rotation vector attribute
    public Vector3f getRotation() {
        return rotation;
    }

    // Setter or the rotation attribute
    public void setRotation(float x, float y, float z) {
        rotation.x = x;
        rotation.y = y;
        rotation.z = z;
    }

    // Adjusts the rotation vector of the camera with a given 3D offset
    public void moveRotation(float offsetX, float offsetY, float offsetZ) {
        rotation.x += offsetX;
        rotation.y += offsetY;
        rotation.z += offsetZ;
    }
}
