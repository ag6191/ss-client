package org.ss.engine.graph;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import org.lwjgl.system.MemoryStack;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;
import static org.lwjgl.stb.STBImage.*;

// Class for a texture object to be applied to a material/mesh object
public class Texture {

    private final int id;

    // Constructor for the Texture class with a given filename
    public Texture(String filename) throws Exception {
        this(loadTexture(filename));
    }

    // Constructor for the Texture class with a given integer id
    public Texture(int id) {
        this.id = id;
    }

    // Binds the texture to opengl program
    public void bind() {
        glBindTexture(GL_TEXTURE_2D, id);
    }

    // Getter for the id attribute
    public int getId() {
        return id;
    }

    // Static method that loads a texture from a given filename and returns the texture id
    private static int loadTexture(String fileName) throws Exception {
        int width;
        int height;
        ByteBuffer buf;

        // Load texture from file
        try (MemoryStack stack = MemoryStack.stackPush()) {
            IntBuffer w = stack.mallocInt(1);
            IntBuffer h = stack.mallocInt(1);
            IntBuffer channels = stack.mallocInt(1);

            buf = stbi_load(fileName, w, h, channels, 4);
            if (buf == null) {
                throw new Exception("Image file [" + fileName + "] not leaded: " + stbi_failure_reason());
            }

            width = w.get();
            height = h.get();
        }

        // Create and bind a new opengl texture
        int textureId = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, textureId);

        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buf);

        glGenerateMipmap(GL_TEXTURE_2D);

        stbi_image_free(buf);

        return textureId;
    }

    // Deletes the texture from the opengl program
    public void cleanup() {
        glDeleteTextures(id);
    }
}
