package org.ss.engine.graph;

import org.joml.Vector4f;
import java.io.*;

// Class for a (textured and colored) material to be applied on top of meshes
public class Material {

    private static final Vector4f DEFAULT_COLOUR = new Vector4f(1.0f, 1.0f, 1.0f, 1.0f);

    private Vector4f ambientColour;

    private Vector4f diffuseColour;

    private Vector4f specularColour;

    private float reflectance;

    private Texture texture;

    // Constructor for the Material class with default colors and no texture
    public Material() {
        this.ambientColour = DEFAULT_COLOUR;
        this.diffuseColour = DEFAULT_COLOUR;
        this.specularColour = DEFAULT_COLOUR;
        this.texture = null;
        this.reflectance = 0;
    }

    // Constructor for the Material class with a given set of color vectors and no texture
    public Material(Vector4f colour, float reflectance) {
        this(colour, colour, colour, null, reflectance);
    }

    // Constructor for the Material class with a given texture and default colors
    public Material(Texture texture) {
        this(DEFAULT_COLOUR, DEFAULT_COLOUR, DEFAULT_COLOUR, texture, 0);
    }

    // Constructor for the Material class with a given texture, reflectance, and default colors
    public Material(Texture texture, float reflectance) {
        this(DEFAULT_COLOUR, DEFAULT_COLOUR, DEFAULT_COLOUR, texture, reflectance);
    }

    // Constructor for the Material class with a given set of color vectors and a texture
    public Material(Vector4f ambientColour, Vector4f diffuseColour, Vector4f specularColour, Texture texture, float reflectance) {
        this.ambientColour = ambientColour;
        this.diffuseColour = diffuseColour;
        this.specularColour = specularColour;
        this.texture = texture;
        this.reflectance = reflectance;
    }

    // Constructor for the Material class from a given filename
    public Material (String mtlFile){
        File materialFile = new File(mtlFile);
        String line;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(materialFile));
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    // Getter for the ambient color vector attribute
    public Vector4f getAmbientColour() {
        return ambientColour;
    }

    // Setter for the ambient color vector attribute
    public void setAmbientColour(Vector4f ambientColour) {
        this.ambientColour = ambientColour;
    }

    // Getter for the diffuse color vector attribute
    public Vector4f getDiffuseColour() {
        return diffuseColour;
    }

    // Setter for the diffuse color vector attribute
    public void setDiffuseColour(Vector4f diffuseColor) {
        this.diffuseColour = diffuseColor;
    }

    // Getter for the specular color vector attribute
    public Vector4f getSpecularColour() {
        return specularColour;
    }

    // Setter for the specular color vector attribute
    public void setSpecularColour(Vector4f specularColour) {
        this.specularColour = specularColour;
    }

    // Getter for the reflectance attribute
    public float getReflectance() {
        return reflectance;
    }

    // Setter for the reflectance attribute
    public void setReflectance(float reflectance) {
        this.reflectance = reflectance;
    }

    // Returns true if texture object attribute is not null
    public boolean isTextured() {
        return this.texture != null;
    }

    // Getter for the texture object attribute
    public Texture getTexture() {
        return texture;
    }

    // Setter for the texture object attribute
    public void setTexture(Texture texture) {
        this.texture = texture;
    }

}
