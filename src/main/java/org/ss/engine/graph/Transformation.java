package org.ss.engine.graph;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.ss.engine.phys.PhysicsObject;
import org.ss.game.object.GameObject;
import org.ss.game.object.SkyboxObject;

// Class to handle the math behind the rendering transformations
public class Transformation {

    private final Matrix4f projectionMatrix;

    private final Matrix4f modelViewMatrix;

    private final Matrix4f viewMatrix;

    // Constructor for the Transformation class
    public Transformation() {
        modelViewMatrix = new Matrix4f();
        projectionMatrix = new Matrix4f();
        viewMatrix = new Matrix4f();
    }

    // Getter for a calculated projection matrix
    public final Matrix4f getProjectionMatrix(float fov, float width, float height, float zNear, float zFar) {
        return projectionMatrix.setPerspective(fov, width / height, zNear, zFar);
    }

    // Getter for a calculated model-view matrix of a Skybox object
    public Matrix4f getModelViewMatrix(SkyboxObject skybox, Matrix4f viewMatrix) {
        Vector3f rotation = skybox.getRotation();
        modelViewMatrix.identity().translate(skybox.getPosition())
                .rotateX((float)Math.toRadians(-rotation.x))
                .rotateY((float)Math.toRadians(-rotation.y))
                .rotateZ((float)Math.toRadians(-rotation.z))
                .scale(skybox.getScale());

        Matrix4f viewCurr = new Matrix4f(viewMatrix);
        return viewCurr.mul(modelViewMatrix);
    }

    // Getter for a calculated model-view matrix of a given PhysicsObject
    public Matrix4f getModelViewMatrix(PhysicsObject physicsObject, Matrix4f viewMatrix) {
        Vector3f rotation = physicsObject.getRotation();
        modelViewMatrix.identity().translate(physicsObject.getPosition())
                .rotateX((float)Math.toRadians(-rotation.x))
                .rotateY((float)Math.toRadians(-rotation.y))
                .rotateZ((float)Math.toRadians(-rotation.z))
                .scale(physicsObject.getScale());

        Matrix4f viewCurr = new Matrix4f(viewMatrix);
        return viewCurr.mul(modelViewMatrix);
    }

    // Getter for a calculate view matrix with respect to a given camera
    public Matrix4f getViewMatrix(Camera camera) {
        Vector3f cameraPos = camera.getPosition();
        Vector3f rotation = camera.getRotation();

        viewMatrix.identity();

        // Allow camera to rotate the position
        viewMatrix.rotate((float)Math.toRadians(rotation.x), new Vector3f(1, 0, 0)).rotate((float)Math.toRadians(rotation.y), new Vector3f(0, 1, 0));

        // Translate wrt position
        viewMatrix.translate(-cameraPos.x, -cameraPos.y, -cameraPos.z);
        return viewMatrix;
    }
}
