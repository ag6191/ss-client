package org.ss.engine.graph.light;

import org.joml.Vector3f;

// Class for a spot light source
public class SpotLight {

    private PointLight pointLight;

    private Vector3f coneDirection;

    private float cutOff;

    // Constructor for a SpotLight object with given attributes
    public SpotLight(PointLight pointLight, Vector3f coneDirection, float cutOffAngle) {
        this.pointLight = pointLight;
        this.coneDirection = coneDirection;
        setCutOffAngle(cutOffAngle);
    }

    // Constructor for a SpotLight object copied from another SpotLight instance
    public SpotLight(SpotLight spotLight) {
        this(new PointLight(spotLight.getPointLight()),
                new Vector3f(spotLight.getConeDirection()),
                0);
        setCutOff(spotLight.getCutOff());
    }

    // Getter for the PointLight attribute
    public PointLight getPointLight() {
        return pointLight;
    }

    // Setter for the PointLight attribute
    public void setPointLight(PointLight pointLight) {
        this.pointLight = pointLight;
    }

    // Getter for the cone direction vector attribute
    public Vector3f getConeDirection() {
        return coneDirection;
    }

    // Setter for the cone direction vector attribute
    public void setConeDirection(Vector3f coneDirection) {
        this.coneDirection = coneDirection;
    }

    // Getter for the cut off attribute
    public float getCutOff() {
        return cutOff;
    }

    // Setter for the cut off attribute
    public void setCutOff(float cutOff) {
        this.cutOff = cutOff;
    }

    // setter for the cut off angle attribute
    public final void setCutOffAngle(float cutOffAngle) {
        this.setCutOff( (float) Math.cos(Math.toRadians(cutOffAngle) ) );
    }
}
