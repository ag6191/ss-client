package org.ss.engine.graph.light;

import org.joml.Vector3f;

// Class for a point light source
public class PointLight {

    private Vector3f color;

    private Vector3f position;

    protected float intensity;

    private Attenuation attenuation;

    // Constructor for the PointLight class
    public PointLight(Vector3f color, Vector3f position, float intensity) {
        attenuation = new Attenuation(1, 0, 0);
        this.color = color;
        this.position = position;
        this.intensity = intensity;
    }

    // Constructor for the PointLight class with a given attenuation
    public PointLight(Vector3f color, Vector3f position, float intensity, Attenuation attenuation) {
        this(color, position, intensity);
        this.attenuation = attenuation;
    }

    // Constructor for the PointLight class that creates a copy of a PointLight object
    public PointLight(PointLight pointLight) {
        this(new Vector3f(pointLight.getColor()), new Vector3f(pointLight.getPosition()),
                pointLight.getIntensity(), pointLight.getAttenuation());
    }

    // Getter for color attribute
    public Vector3f getColor() {
        return color;
    }

    // Setter for color attribute
    public void setColor(Vector3f color) {
        this.color = color;
    }

    // Getter for position attribute
    public Vector3f getPosition() {
        return position;
    }

    // Setter for position attribute
    public void setPosition(Vector3f position) {
        this.position = position;
    }

    // Getter for the intensity attribute
    public float getIntensity() {
        return intensity;
    }

    // Setter for the intensity attribute
    public void setIntensity(float intensity) {
        this.intensity = intensity;
    }

    // Getter for the attenuation object attribute
    public Attenuation getAttenuation() {
        return attenuation;
    }

    // Setter for the attenuation object attribute
    public void setAttenuation(Attenuation attenuation) {
        this.attenuation = attenuation;
    }

    // Class for a light attenuation and its parameters
    public static class Attenuation {

        private float constant;

        private float linear;

        private float exponent;

        // Constructor for the Attenuation class
        public Attenuation(float constant, float linear, float exponent) {
            this.constant = constant;
            this.linear = linear;
            this.exponent = exponent;
        }

        // Getter for constant attribute
        public float getConstant() {
            return constant;
        }

        // Setter for constant attribute
        public void setConstant(float constant) {
            this.constant = constant;
        }

        // Getter for linear attribute
        public float getLinear() {
            return linear;
        }

        // Setter for linear attribute
        public void setLinear(float linear) {
            this.linear = linear;
        }

        // Getter for exponent attribute
        public float getExponent() {
            return exponent;
        }

        // Setter for exponent attribute
        public void setExponent(float exponent) {
            this.exponent = exponent;
        }
    }
}
