package org.ss.engine.graph.light;

import org.joml.Vector3f;
import org.ss.engine.loader.LightLoader;

// Class for a collection of multiple types of lights within the scene
public class SceneLight {
    private Vector3f ambientLight;

    private PointLight[] pointLightList;

    private SpotLight[] spotLightList;

    private DirectionalLight directionalLight;

    // Constructor for a SceneLight object that initializes default lights
    public SceneLight(){
        Vector3f whiteLightColor = new Vector3f(1,1,1);
        this.setAmbientLight(new Vector3f(0.3f, 0.3f, 0.3f));
        this.setPointLightList(new PointLight[]{
                LightLoader.spawnPointLight(whiteLightColor, new Vector3f(0, 0, 1), 1.0f, 1.0f)});
        this.setSpotLightList(new SpotLight[]{
                LightLoader.spawnSpotLight(whiteLightColor, new Vector3f(0, 0.0f, 10f), new Vector3f(0, 0, -1), 1.0f, 140, 0.02f)});
        this.setDirectionalLight(LightLoader.spawnDirectionalLight(whiteLightColor, new Vector3f(-1, 0, 0), 1.0f));
    }

    // Constructor for a SceneLight object with given light lists
    public SceneLight(Vector3f ambientLight, PointLight[] pointLightList, SpotLight[] spotLightList, DirectionalLight directionalLight ){
        this.ambientLight = ambientLight;
        this.pointLightList = pointLightList;
        this.spotLightList = spotLightList;
        this.directionalLight = directionalLight;
    }

    // Getter for ambient light attribute
    public Vector3f getAmbientLight() {
        return ambientLight;
    }

    // Setter for ambient light attribute
    public void setAmbientLight(Vector3f ambientLight) {
        this.ambientLight = ambientLight;
    }

    // Getter for PointLight object collection attribute
    public PointLight[] getPointLightList() {
        return pointLightList;
    }

    // Setter for PointLight object collection attribute
    public void setPointLightList(PointLight[] pointLightList) {
        this.pointLightList = pointLightList;
    }

    // Getter for SpotLight object collection attribute
    public SpotLight[] getSpotLightList() {
        return spotLightList;
    }

    // Setter for SpotLight object collection attribute
    public void setSpotLightList(SpotLight[] spotLightList) {
        this.spotLightList = spotLightList;
    }

    // Getter for DirectionalLight object attribute
    public DirectionalLight getDirectionalLight() {
        return directionalLight;
    }

    // Setter for DirectionalLight object attribute
    public void setDirectionalLight(DirectionalLight directionalLight) {
        this.directionalLight = directionalLight;
    }
}
