package org.ss.engine.graph.light;

import org.joml.Vector3f;

// Class for a directional light source
public class DirectionalLight {

    private Vector3f color;

    private Vector3f direction;

    private float intensity;

    // Constructor for the DirectionalLight class
    public DirectionalLight(Vector3f color, Vector3f direction, float intensity) {
        this.color = color;
        this.direction = direction;
        this.intensity = intensity;
    }

    // Constructor for the DirectionalLight class with default values
    public DirectionalLight(DirectionalLight light) {
        this(new Vector3f(light.getColor()), new Vector3f(light.getDirection()), light.getIntensity());
    }

    // Getter for the color attribute
    public Vector3f getColor() {
        return color;
    }

    // Setter for the color attribute
    public void setColor(Vector3f color) {
        this.color = color;
    }

    // Getter for the direction attribute
    public Vector3f getDirection() {
        return direction;
    }

    // Setter for the direction attribute
    public void setDirection(Vector3f direction) {
        this.direction = direction;
    }

    // Getter for the intensity attribute
    public float getIntensity() {
        return intensity;
    }

    // Setter for the intensity attribute
    public void setIntensity(float intensity) {
        this.intensity = intensity;
    }
}
