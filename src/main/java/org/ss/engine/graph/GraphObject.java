package org.ss.engine.graph;

import org.joml.Vector3f;

// Class for a graphic object that is interpreted by the renderer using a mesh and physical attributes
public class GraphObject {

    private Mesh mesh;

    private float scale;

    private Vector3f position = null;

    private Vector3f rotation = null;

    // Constructor for the GraphObject class witha given mesh, scale, and position
    public GraphObject(Mesh mesh, float scale, Vector3f position) {
        this.mesh = mesh;
        this.scale = scale;
        this.position = new Vector3f(position);
        this.rotation = new Vector3f();
    }

    // Getter for the position attribute
    public Vector3f getPosition() {
        return position;
    }

    // Setter for the position attribute
    public void setPosition(float x, float y, float z) {
        this.position.x = x;
        this.position.y = y;
        this.position.z = z;
    }

    // Getter for the scale attribute
    public float getScale() {
        return scale;
    }

    // Setter for the scale attribute
    public void setScale(float scale) {
        this.scale = scale;
    }

    // Getter for the rotation attribute
    public Vector3f getRotation() {
        return rotation;
    }

    // Setter for the rotation attribute
    public void setRotation(float x, float y, float z) {
        this.rotation.x = x;
        this.rotation.y = y;
        this.rotation.z = z;
    }

    // Getter for the mesh object attribute
    public Mesh getMesh() {
        return mesh;
    }

    // Setter for the mesh object attribute
    public void setMesh(Mesh mesh) {
        this.mesh = mesh;
    }
}
