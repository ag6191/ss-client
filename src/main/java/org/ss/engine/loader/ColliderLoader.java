package org.ss.engine.loader;

import org.joml.Vector3f;
import org.ss.engine.phys.Collider;
import org.ss.engine.phys.ColliderType;
import org.ss.engine.phys.body.BoundingSphere;

// Class responsible for creating and setting up colliders
public class ColliderLoader {

    // Returns a new collider of the given enum collider type
    public static Collider initCollider(ColliderType colliderType, float scale, Vector3f position) {
        switch (colliderType) {
            case BOUNDING_SPHERE:
                return initBoundingSphere(scale, position);

            default:
                return null;
        }
    }

    // Returns a new BoundingSphere class collider
    private static BoundingSphere initBoundingSphere(float scale, Vector3f position) {
        return new BoundingSphere(position, scale*1.4f);
    }
}
