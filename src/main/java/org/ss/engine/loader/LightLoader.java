package org.ss.engine.loader;

import org.joml.Vector3f;
import org.ss.engine.graph.light.*;

// Class responsible for efficently loading and setting up various light objects to be used in the scene
public class LightLoader {

    // Returns a new PointLight instance with respect to given arguments
    public static PointLight spawnPointLight(Vector3f color, Vector3f position, float intensity, float attExponent) {
        PointLight pointLight = new PointLight(color, position, intensity);
        PointLight.Attenuation att = new PointLight.Attenuation(0.0f, 0.0f, attExponent);
        pointLight.setAttenuation(att);
        return pointLight;
    }

    // Returns a new PointLight instance with respect to given arguments and default values
    public static PointLight spawnPointLight(Vector3f position, float intensity) {
        return spawnPointLight(new Vector3f(1,1,1), position, intensity, 1.0f);
    }

    // Returns a new SpotLight instance with respect to given arguments
    public static SpotLight spawnSpotLight(Vector3f color, Vector3f position, Vector3f coneDirection, float intensity, float cutoffArc, float attExponent) {
        PointLight pointLight = spawnPointLight(color, position, intensity, attExponent);
        float cutoff = (float) Math.cos(Math.toRadians(cutoffArc));
        return new SpotLight(pointLight, coneDirection, cutoff);
    }

    // Returns a new DirectionalLight instance with respect to given arguments
    public static DirectionalLight spawnDirectionalLight(Vector3f color, Vector3f position, float intensity) {
        return new DirectionalLight(color, position, intensity);
    }
}
