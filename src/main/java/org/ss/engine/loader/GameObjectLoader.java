package org.ss.engine.loader;

import org.joml.Vector3f;
import org.ss.engine.Utils;
import org.ss.engine.graph.Mesh;
import org.ss.game.object.GameObject;
import org.ss.game.object.ShipObject;

// Class responsible for efficiently creating and setting up GameObjects
public class GameObjectLoader {

    // Returns a new GameObject instance from given arguments
    public static GameObject spawnGameObject(Mesh mesh, float scale, Vector3f position) {
        GameObject gameObject = new GameObject(mesh, scale, position);
        return gameObject;
    }

    // Returns a new collection of existing GameObjects and a newly spawned GameObject instance
    public static GameObject[] spawnGameObject(GameObject[] gameObjectArr, Mesh mesh, float scale, Vector3f position) {
        return insertGameObject(gameObjectArr, spawnGameObject(mesh, scale, position));
    }

    // Returns a new ShipObject instance from given arguments
    public static ShipObject spawnShipObject(Mesh mesh, float scale, Vector3f position) throws Exception {
        ShipObject shipObject = new ShipObject(mesh, scale, position);
        return shipObject;
    }

    // Randomizes a given GameObject's velocity
    public static void randomizeGameObjectVelocity(GameObject gameObject, float min, float max) {
        gameObject.setVelocity(
                Utils.randFloat(min, max),
                Utils.randFloat(min, max),
                Utils.randFloat(min, max));
    }

    // Returns a newly created GameObject instance in a certain area
    public static GameObject spawnGameObjectInArea(Mesh mesh, float scale, int xAreaDim, int yAreaDim, int zAreaDim) {

        int x = Utils.randInt(-1 * xAreaDim / 2, xAreaDim / 2);
        int y = Utils.randInt(-1 * yAreaDim / 2, yAreaDim / 2);
        int z = Utils.randInt(-1 * zAreaDim / 2, zAreaDim / 2);

        return spawnGameObject(mesh, scale, new Vector3f(x, y, z));
    }

    // Inserts a given GameObject into a collection of GameObjects
    public static GameObject[] insertGameObject(GameObject[] gameObjectArr, GameObject gameObject) {
        GameObject[] newGameObjectArr = new GameObject[gameObjectArr.length + 1];
        System.arraycopy(gameObjectArr, 0, newGameObjectArr, 0, gameObjectArr.length);
        newGameObjectArr[gameObjectArr.length] = gameObject;
        return newGameObjectArr;
    }
}
