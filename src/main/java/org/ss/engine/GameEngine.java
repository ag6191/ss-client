package org.ss.engine;

// Class that handles the core logic of the engine behind the game
public class GameEngine implements Runnable{

    public static final int TARGET_FPS = 75;

    public static final int TARGET_UPS = 30;

    private final Window window;

    private final Timer timer;

    private final IGameLogic gameLogic;

    private final MouseInput mouseInput;

    private final Thread gameLoopThread;

    // Constructor for the GameEngine class
    public GameEngine(String windowTitle, int width, int height, boolean vSync, IGameLogic gameLogic) throws Exception {
        window = new Window(windowTitle, width, height, vSync);
        mouseInput = new MouseInput();
        gameLoopThread = new Thread(this, "GAME_LOOP_THREAD");
        this.gameLogic = gameLogic;
        timer = new Timer();
    }

    // Method to start the game engine on a specific thread determined by the OS used
    public void start() {
        String osName = System.getProperty("os.name");
        if (osName.contains("Mac")) {
            gameLoopThread.run();
        } else {
            gameLoopThread.start();
        }
    }

    // Overwritten method to run the thread for MacOS
    @Override
    public void run() {
        try {
            init();
            gameLoop();
        } catch (Exception excp) {
            excp.printStackTrace();
        } finally {
            cleanup();
        }
    }

    // Initializes the window, timer, mouse, and game logic
    protected void init() throws Exception {
        window.init();
        timer.init();
        mouseInput.init(window);
        gameLogic.init(window);
    }

    // Main game loop that handles displaying and updating everything at a fixed frame-rate interval
    protected void gameLoop() {
        float elapsedTime;
        float accumulator = 0f;
        float interval = 1f / TARGET_UPS;

        boolean running = true;
        while (running && !window.windowShouldClose()) {
            elapsedTime = timer.getElapsedTime();
            accumulator += elapsedTime;

            // Get input
            input();

            // Update if time passed is less than the frame interval
            while (accumulator >= interval) {
                update(interval);
                accumulator -= interval;
            }

            // Render items in scene
            render();

            // Sync window
            if (!window.isvSync()) {
                sync();
            }
        }
    }

    // Calls game logic cleanup method to destroy everything
    protected void cleanup() {
        gameLogic.cleanup();
    }

    // Syncs the frame-rate for smoother gameplay
    private void sync() {
        float loopSlot = 1f / TARGET_FPS;
        double endTime = timer.getLastLoopTime() + loopSlot;
        while (timer.getTime() < endTime) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException ie) {

            }
        }
    }

    // Collects mouse and game logic input
    protected void input() {
        mouseInput.input();
        gameLogic.input(window, mouseInput);
    }

    // Updates scene with respect to the game logic update method
    protected void update(float interval) {
        gameLogic.update(interval, mouseInput);
    }

    // Renders items in game logic and updates the window display
    protected void render() {
        gameLogic.render(window);
        window.update();
    }
}