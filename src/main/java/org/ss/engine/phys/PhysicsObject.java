package org.ss.engine.phys;

import org.joml.Vector3f;
import org.ss.engine.Utils;
import org.ss.engine.graph.GraphObject;
import org.ss.engine.graph.Mesh;
import org.ss.engine.loader.ColliderLoader;

// Class for a physics object to be used and manipulated by the physics engine
public class PhysicsObject extends GraphObject {

    private Collider collider;

    private Vector3f velocity;

    private PhysicsObject next = null;

    private PhysicsObject prev = null;

    private PhysicsObjectManager physicsObjectManager = null;

    // Constructor for the PhysicsObject class
    public PhysicsObject(Mesh mesh, float scale, Vector3f position, ColliderType colliderType) {
        super(mesh, scale, position);
        this.velocity = new Vector3f();
        this.collider = ColliderLoader.initCollider(colliderType, scale, position);
    }

    // Getter for the collider object attribute
    public Collider getCollider() {
        return collider;
    }

    // Getter for the velocity vector attribute
    public Vector3f getVelocity() {
        return velocity;
    }

    // Setter for the velocity vector attribute from x,y,x components
    public void setVelocity(float x, float y, float z) {
        this.velocity.x = x;
        this.velocity.y = y;
        this.velocity.z = z;
    }

    // Setter for the velocity vector attribute from another vector
    public void setVelocity(Vector3f velocity) {
        this.velocity.set(velocity);
    }

    // Calculates the movement offset with respect to the velocity and a fixed frame step and then moves the position
    public void movePosition() {
        float offsetX = velocity.x * Utils.OBJECT_DELTA_STEP;
        float offsetY = velocity.y * Utils.OBJECT_DELTA_STEP;
        float offsetZ = velocity.z * Utils.OBJECT_DELTA_STEP;
        movePosition(offsetX, offsetY, offsetZ);
    }

    // Moves the position of the PhysicsObject with respect to an offset
    public void movePosition(float offsetX, float offsetY, float offsetZ) {
        float dx = 0;
        float dy = 0;
        float dz = 0;
        Vector3f position = super.getPosition();
        if (offsetZ != 0) {
            dx += (float) Math.sin(Math.toRadians(super.getRotation().y)) * -1.0f * offsetZ;
            dz += (float) Math.cos(Math.toRadians(super.getRotation().y)) * offsetZ;
        }
        if (offsetX != 0) {
            dx += (float) Math.sin(Math.toRadians(super.getRotation().y - 90)) * -1.0f * offsetX;
            dz += (float) Math.cos(Math.toRadians(super.getRotation().y - 90)) * offsetX;
        }
        dy += offsetY;
        super.setPosition(position.x + dx, position.y + dy, position.z + dz);

        if (collider != null) {
            collider.setPosition(super.getPosition());
        }
    }

    // Moves the rotation vector with respect to a given offset
    public void moveRotation(float offsetX, float offsetY, float offsetZ) {
        Vector3f rotation = super.getRotation();
        super.setRotation(rotation.x + offsetX, rotation.y + offsetY, rotation.z + offsetZ);
    }

    // Setter for the next PhysicsObject node to a given PhysicsObject
    public void setNext(PhysicsObject physicsObject) {
        this.next = physicsObject;
    }

    // Getter for the next PhysicsObject node
    public PhysicsObject getNext() {
        return next;
    }

    // Setter for the previous PhysicsObject node to a given PhysicsObject
    public void setPrev(PhysicsObject physicsObject) {
        this.prev = physicsObject;
    }

    // Getter for the previous PhysicsObject node
    public PhysicsObject getPrev() {
        return prev;
    }

    // Setter for the PhysicsObjectManager attribute
    public void setPhysicsObjectManager(PhysicsObjectManager physicsObjectManager) {
        this.physicsObjectManager = physicsObjectManager;
    }

    // Getter for the PhysicsObjectManager attribute
    public PhysicsObjectManager getPhysicsObjectManager() {
        return physicsObjectManager;
    }
}
