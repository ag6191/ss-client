package org.ss.engine.phys.body;

import org.joml.Vector3f;
import org.ss.engine.phys.Collider;
import org.ss.engine.phys.IntersectData;

// Class for a bounding sphere to be used for collisions
public class BoundingSphere extends Collider {

    private final Vector3f position;

    private final float radius;

    // Constructor for the BoundingSphere class
    public BoundingSphere(Vector3f position, float radius) {
        this.position = position;
        this.radius = radius;
    }

    // Returns intersect data of two bounding spheres
    public IntersectData intersectBoundingSphere(BoundingSphere other) {
        float radiusDistance = radius + other.getRadius();
        Vector3f direction = new Vector3f(
                other.getPosition().x - position.x,
                other.getPosition().y - position.y,
                other.getPosition().z - position.z
        );
        float centerDistance = direction.length();
        float distance = centerDistance - radiusDistance;
        direction.div(centerDistance);

        return new IntersectData(distance < 0, direction.mul(distance));
    }

    // Getter for the radius attribute
    public float getRadius() {
        return radius;
    }

    // Adjusts the position of the bounding sphere with respect to a translation
    public void transform(Vector3f translation) {
        position.add(translation);
    }

    // Getter for the position vector attribute
    public Vector3f getPosition() {
        return position;
    }

    // Setter for the position vector attribute
    public void setPosition(Vector3f position) {
        this.position.set(position);
    }
}
