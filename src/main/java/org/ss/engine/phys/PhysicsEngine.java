package org.ss.engine.phys;

import org.ss.game.object.GameObject;
import org.ss.game.object.ParticleObject;
import org.ss.game.object.ProjectileObject;
import org.ss.game.object.ShipObject;

// Class for the physics engine to handle game objects and collisions
public class PhysicsEngine {

    private final PhysicsObjectManager gameObjectManager;

    private final PhysicsObjectManager particleObjectManager;

    private final PhysicsObjectManager projectileObjectManager;

    // Constructor for the PhysicsEngine class
    public PhysicsEngine() {
        this.gameObjectManager = new PhysicsObjectManager();
        this.particleObjectManager = new PhysicsObjectManager();
        this.projectileObjectManager = new PhysicsObjectManager();
    }

    // Adds a given GameObject to the respective manager
    public void addGameObject(GameObject gameObject) {
        gameObjectManager.addLast((PhysicsObject) gameObject);
    }

    // Adds a collection of GameObjects to the respective manager
    public void addGameObjectArr(GameObject[] gameObjectArr) {
        for (GameObject gameObject : gameObjectArr) {
            addGameObject(gameObject);
        }
    }

    // Adds a given ParticleObject to the respective manager
    public void addParticleObject(ParticleObject particleObject) {
        particleObjectManager.addLast((PhysicsObject) particleObject);
    }

    // Adds a collection of ParticleObjects to the respective manager
    public void addParticleObjectArr(ParticleObject[] particleObjectArr) {
        for (ParticleObject particleObject : particleObjectArr) {
            addParticleObject(particleObject);
        }
    }

    // Adds a given ProjectileObject to the respective manager
    public void addProjectileObject(ProjectileObject projectileObject) {
        projectileObjectManager.addLast((PhysicsObject) projectileObject);
    }

    // Adds a collection of ProjectileObjects to the respective manager
    public void addProjectileObjectArr(ProjectileObject[] projectileObjectArr) {
        for (ProjectileObject projectileObject : projectileObjectArr) {
            addProjectileObject(projectileObject);
        }
    }

    // Getter for the a collection from the GameObject manager
    public PhysicsObject[] getGameObjectArr() {
        return gameObjectManager.toArray();
    }

    // Getter for the a collection from the ParticleObject manager
    public PhysicsObject[] getParticleObjectArr() {
        return particleObjectManager.toArray();
    }

    // Getter for the a collection from the ProjectileObject manager
    public PhysicsObject[] getProjectileObjectArr() {
        return projectileObjectManager.toArray();
    }

    // Preforms a cleanup on all GameObjects in each manager
    public void cleanup() {
        PhysicsObject currPhysicsObject = gameObjectManager.getFirst();
        while (currPhysicsObject != null) {
            currPhysicsObject.getMesh().cleanup();
            currPhysicsObject = currPhysicsObject.getNext();
        }
        currPhysicsObject = particleObjectManager.getFirst();
        while (currPhysicsObject != null) {
            currPhysicsObject.getMesh().cleanup();
            currPhysicsObject = currPhysicsObject.getNext();
        }
        currPhysicsObject = projectileObjectManager.getFirst();
        while (currPhysicsObject != null) {
            currPhysicsObject.getMesh().cleanup();
            currPhysicsObject = currPhysicsObject.getNext();
        }
    }

    // Updates all objects
    public void updateObjects() {
        updateGameObjects();
        updateProjectileObjects();
        updateParticleObjects();
    }

    // Updates object positions in GameObject manager
    private void updateGameObjects() {
        PhysicsObject currGameObject = gameObjectManager.getFirst();
        while (currGameObject != null) {
            currGameObject.movePosition();
            currGameObject = currGameObject.getNext();
        }
    }

    // Updates object positions in ProjectileObject and deletes them if they are out of range
    private void updateProjectileObjects() {
        ProjectileObject currProjectileObject = (ProjectileObject) projectileObjectManager.getFirst();
        while (currProjectileObject != null) {
            if (currProjectileObject.outOfRange()) {
                PhysicsObjectManager.remove(currProjectileObject);
            } else {
                currProjectileObject.movePosition();
            }
            currProjectileObject = (ProjectileObject) currProjectileObject.getNext();
        }
    }

    // Updates object positions in ParticleObject and deletes them if they have exceeded their duration
    private void updateParticleObjects() {
        ParticleObject currParticleObject = (ParticleObject) particleObjectManager.getFirst();
        while (currParticleObject != null) {
            if (currParticleObject.canDeleteParticle()) {
                PhysicsObjectManager.remove(currParticleObject);
            } else {
                currParticleObject.movePosition();
            }
            currParticleObject = (ParticleObject) currParticleObject.getNext();
        }
    }

    // Detects and handles object collisions
    public void detectCollisions() {
        detectProjectileObjectCollisions();
    }

    // Detects and handles ProjectileObject and GameObject collisions
    private void detectProjectileObjectCollisions() {
        PhysicsObject currProjectileObject = projectileObjectManager.getFirst();
        PhysicsObject currGameObject;
        while (currProjectileObject != null) {
            currGameObject = gameObjectManager.getFirst();
            while (currGameObject != null) {
                if (!(currGameObject instanceof ShipObject)) {
                    IntersectData intersectData = currGameObject.getCollider().intersect(currProjectileObject.getCollider());
                    if (intersectData.doesIntersect()) {
                        handleProjectileObjectCollisions((GameObject) currGameObject, (ProjectileObject) currProjectileObject);
                    }
                }
                currGameObject = currGameObject.getNext();
            }
            currProjectileObject = currProjectileObject.getNext();
        }
    }

    // Handles collisions between ProjectileObjects and GameObjects by deleting them and adding particles if applicable
    private void handleProjectileObjectCollisions(GameObject gameObject, ProjectileObject projectileObject) {
        gameObject.reduceHealth(projectileObject.getDamage());
        PhysicsObjectManager.remove(projectileObject);
        if (gameObject.getHealth() < 0) {
            PhysicsObjectManager.remove(gameObject);
            particleObjectManager.addArrLast(gameObject.getExplosionParticleArr());
        }
    }
}
