package org.ss.engine.phys;

import org.joml.Vector3f;

// Class that stores the intersect data of a collision
public class IntersectData {

    private final boolean doesIntersect;

    private final Vector3f direction;

    public IntersectData(boolean doesIntersect, Vector3f direction) {
        this.doesIntersect = doesIntersect;
        this.direction = direction;
    }

    public boolean doesIntersect() {
        return doesIntersect;
    }

    public Vector3f getDistance() {
        return direction;
    }
}
