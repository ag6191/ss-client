package org.ss.engine.phys;

import org.joml.Vector3f;
import org.ss.engine.phys.body.BoundingSphere;

// Abstract class for a bounding body collider
public abstract class Collider {

    // Adjusts the position of the bounding sphere with respect to a translation
    public abstract void transform(Vector3f translation);

    // Getter for the position vector attribute
    public abstract Vector3f getPosition();

    // Setter for the position vector attribute
    public abstract void setPosition(Vector3f vector3f);

    // Returns intersect data of two colliders
    public IntersectData intersect(Collider other) {
        if (other == null) {
            return new IntersectData(false, new Vector3f());
        }

        // Collision between two bounding spheres
        if (this instanceof BoundingSphere && other instanceof BoundingSphere) {
            return ((BoundingSphere) this).intersectBoundingSphere((BoundingSphere) other);
        } else {
            return new IntersectData(false, new Vector3f());
        }
    }
}

