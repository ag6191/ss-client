package org.ss.engine.phys;

// Class that manages a linked list of PhysicsObjects for a faster management of items in the game
public class PhysicsObjectManager {

    private int countPhysicsObjects;

    private PhysicsObject head;

    private PhysicsObject tail;

    // Constructor for the PhysicsObjectManager class
    public PhysicsObjectManager() {
        countPhysicsObjects = 0;
        head = null;
        tail = null;
    }

    // Getter for the first PhysicsObject in the linked list
    public PhysicsObject getFirst() {
        return head;
    }

    // Getter for the last PhysicsObject in the linked list
    public PhysicsObject getLast() {
        return tail;
    }

    // Adds a given PhysicsObject to the end of the linked list
    public void addLast(PhysicsObject physicsObject) {
        if (countPhysicsObjects == 0 || tail == null) {
            head = physicsObject;
        } else {
            physicsObject.setPrev(tail);
            physicsObject.setNext(null);
            tail.setNext(physicsObject);
        }
        tail = physicsObject;
        physicsObject.setPhysicsObjectManager(this);
        countPhysicsObjects++;
    }

    // Adds a given PhysicsObject to the front of the linked list
    public void addFirst(PhysicsObject physicsObject) {
        if (countPhysicsObjects == 0 || head == null) {
            head = physicsObject;
            tail = physicsObject;
        } else {
            physicsObject.setNext(head);
            physicsObject.setPrev(null);
            head.setPrev(physicsObject);
            head = physicsObject;
        }
        physicsObject.setPhysicsObjectManager(this);
        countPhysicsObjects++;
    }

    // Adds a collection of PhysicsObjects to the front of the linked list
    public void addArrFirst(PhysicsObject[] physicsObjectArr) {
        for (PhysicsObject physicsObject : physicsObjectArr) {
            addFirst(physicsObject);
        }
    }

    // Adds a collection of PhysicsObjects to the end of the linked list
    public void addArrLast(PhysicsObject[] physicsObjectArr) {
        for (PhysicsObject physicsObject : physicsObjectArr) {
            addLast(physicsObject);
        }
    }

    // Removes a PhysicsObject from the linked list at a given index
    public void remove(int physicsObjectIdx) {
        if (countPhysicsObjects == 0 ||
                physicsObjectIdx + 1 > countPhysicsObjects) { return; }

        int currIdx = 0;
        PhysicsObject removalCandidate = head;
        while (currIdx < physicsObjectIdx) {
            removalCandidate = removalCandidate.getNext();
            if (removalCandidate == null) { return; }
            currIdx++;
        }

        remove(removalCandidate);
    }

    // Removes a given PhysicsObject from the link list it is apart of
    public static void remove(PhysicsObject physicsObject) {

        if (physicsObject.getPhysicsObjectManager() == null) { return; }

        if (physicsObject.getPhysicsObjectManager().size() == 1) {
            physicsObject.getPhysicsObjectManager().clear();
            return;
        }

        if (physicsObject.getPrev() != null) {
            physicsObject.getPrev().setNext(physicsObject.getNext());
        } else {
            physicsObject.getPhysicsObjectManager().setHead(physicsObject.getNext());
        }

        if (physicsObject.getNext() != null) {
            physicsObject.getNext().setPrev(physicsObject.getPrev());
        } else {
            physicsObject.getPhysicsObjectManager().setTail(physicsObject.getPrev());
        }

        physicsObject.getPhysicsObjectManager().reduceSize();
    }

    // Removes the first PhysicsObject in the linked list
    public void removeFirst() {
        if (countPhysicsObjects == 0) { return; }
        if (head.getNext() != null) {
            head.getNext().setPrev(null);
            head = head.getNext();
        } else {
            head = null;
            tail = null;
        }
        countPhysicsObjects--;
    }

    // Removes the last PhysicsObject in the linked list
    public void removeLast() {
        if (countPhysicsObjects == 0) { return; }
        if (tail.getPrev() != null) {
            tail.getPrev().setNext(null);
            tail = tail.getPrev();
        } else {
            head = null;
            tail = null;
        }
        countPhysicsObjects--;
    }

    // Removes and returns the a PhysicsObject in the linked list from a given index
    public PhysicsObject pop(int physicsObjectIdx) {
        if (countPhysicsObjects == 0 ||
                physicsObjectIdx + 1 > countPhysicsObjects) { return null; }

        int currIdx = 0;
        PhysicsObject removalCandidate = head;
        while (currIdx < physicsObjectIdx) {
            removalCandidate = removalCandidate.getNext();
            if (removalCandidate == null) { return null; }
            currIdx++;
        }

        removalCandidate.getNext().setPrev(removalCandidate.getPrev());
        removalCandidate.getPrev().setNext(removalCandidate.getNext());
        countPhysicsObjects--;
        return removalCandidate;
    }

    // Removes and returns the first PhysicsObject in the linked list
    public PhysicsObject popFirst() {
        if (countPhysicsObjects == 0) { return null; }
        PhysicsObject removalCandidate = head;
        if (removalCandidate.getNext() != null) {
            removalCandidate.getNext().setPrev(null);
            head = removalCandidate.getNext();
        } else {
            head = null;
            tail = null;
        }
        countPhysicsObjects--;
        return removalCandidate;
    }

    // Removes and returns the last PhysicsObject in the linked list
    public PhysicsObject popLast() {
        if (countPhysicsObjects == 0) { return null; }
        PhysicsObject removalCandidate = tail;
        if (removalCandidate.getPrev() != null) {
            removalCandidate.getPrev().setNext(null);
            tail = removalCandidate.getPrev();
        } else {
            head = null;
            tail = null;
        }
        countPhysicsObjects--;
        return removalCandidate;
    }

    // Returns an array of physics objects in the manager
    public PhysicsObject[] toArray() {
        PhysicsObject[] physicsObjectArr = new PhysicsObject[countPhysicsObjects];
        if (countPhysicsObjects > 0) {
            int currIdx = 0;
            PhysicsObject currPhysicsObject = head;
            while (currPhysicsObject != null) {
                physicsObjectArr[currIdx] = currPhysicsObject;
                currPhysicsObject = currPhysicsObject.getNext();
                currIdx++;
            }
            if (currIdx < countPhysicsObjects) {
                System.out.println("Count is fucked up");
            }
        }
        return physicsObjectArr;
    }

    // Static method that returns an array of PhysicsObjects from a collection of managers
    public static PhysicsObject[] toArray(PhysicsObjectManager[] physicsObjectManagerArr) {
        int countAllPhysicsObjects = 0;
        for (PhysicsObjectManager physicsObjectManager : physicsObjectManagerArr) {
            countAllPhysicsObjects += physicsObjectManager.size();
        }

        int currPhysicsObjectArrIdx = 0;
        PhysicsObject[] physicsObjectArr = new PhysicsObject[countAllPhysicsObjects];

        for (PhysicsObjectManager physicsObjectManager : physicsObjectManagerArr) {
            PhysicsObject currPhysicsObject = physicsObjectManager.getFirst();
            while (currPhysicsObject != null) {
                physicsObjectArr[currPhysicsObjectArrIdx] = currPhysicsObject;
                currPhysicsObject = currPhysicsObject.getNext();
                currPhysicsObjectArrIdx++;
            }
        }
        return physicsObjectArr;
    }

    // Clears out the manager's linked list
    public void clear() {
        countPhysicsObjects = 0;
        head = null;
        tail = null;
    }

    // Returns true if the manager contains the given PhysicsObject in the linked list
    public boolean contains(PhysicsObject physicsObject) {
        PhysicsObject comparisonCandidate = head;
        while (comparisonCandidate != null) {
            if (comparisonCandidate == physicsObject) {
                return true;
            }
            comparisonCandidate = comparisonCandidate.getNext();
        }
        return false;
    }

    // Getter for the size attribute
    public int size() {
        return countPhysicsObjects;
    }

    // Decreases the size attribute by 1
    public void reduceSize() {
        this.countPhysicsObjects -= 1;
    }

    // Increases the size attribute by 1
    public void increaseSize() {
        this.countPhysicsObjects += 1;
    }

    // Setter for the head node
    public void setHead(PhysicsObject physicsObject) {
        this.head = physicsObject;
    }

    // Setter for the tail node
    public void setTail(PhysicsObject physicsObject) {
        this.tail = physicsObject;
    }
}
