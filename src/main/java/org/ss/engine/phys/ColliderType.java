package org.ss.engine.phys;

// Enum for the different collider types
public enum ColliderType {
    BOUNDING_SPHERE,
    NONE;
}
