package org.ss.engine;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

// Class that stores general constants and utility functions for the game
public class Utils {

    public static final float CAMERA_POS_STEP = 0.05f;

    public static final float OBJECT_DELTA_STEP = 0.05f;

    public static final float CAMERA_Y_POS_OFFSET = 0.5f;

    public static final float MOUSE_SENSITIVITY = 0.2f;

    public static final float CAMERA_3PV_DISTANCE = 5.0f;

    public static final float CAMERA_3PV_PITCH = 20.0f;

    // Returns a string result of a loaded resource from a given filename
    public static String loadResource(String fileName) throws Exception {
        String result;
        try (InputStream in = Utils.class.getResourceAsStream(fileName);
             Scanner scanner = new Scanner(in, java.nio.charset.StandardCharsets.UTF_8.name())) {
            result = scanner.useDelimiter("\\A").next();
        }
        return result;
    }

    // Returns a String list of all lines from a given filename
    public static List<String> readAllLines(String filename) throws Exception {
        List<String> list = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(Class.forName(Utils.class.getName()).getResourceAsStream(filename)))) {
            String line;
            while ((line = br.readLine()) != null) {
                list.add(line);
            }
        }
        return list;
    }

    // Returns a random integer within a given range
    public static int randInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    // Returns a random floating point number within a given float range
    public static float randFloat(float min, float max){
        float rand = ThreadLocalRandom.current().nextFloat();
        float range = max - min;
        return (rand * range) - range/2.0f;
    }
}
