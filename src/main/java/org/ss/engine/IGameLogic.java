package org.ss.engine;

// General interface class for game logic
public interface IGameLogic {

    // Initialize game items and scene
    void init(Window window) throws Exception;

    // Collect input
    void input(Window window, MouseInput mouseInput);

    // Update game items and scene
    void update(float interval, MouseInput mouseInput);

    // Render game items
    void render(Window window);

    // Cleanup of everything in game logic scene
    void cleanup();
}
