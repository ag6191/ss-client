package org.ss.engine;

// Class that implements a simple timer
public class Timer {

    private double lastLoopTime;

    // Initializer for the Timer class
    public void init() {
        lastLoopTime = getTime();
    }

    // Returns the current time
    public static double getTime() {
        return System.nanoTime() / 1_000_000_000.0;
    }

    // Returns the elapsed time from the last loop time attribute
    public float getElapsedTime() {
        double time = getTime();
        float elapsedTime = (float) (time - lastLoopTime);
        lastLoopTime = time;
        return elapsedTime;
    }

    // Getter for the last loop time attribute
    public double getLastLoopTime() {
        return lastLoopTime;
    }
}
