package org.ss.engine;

import org.ss.engine.graph.light.SceneLight;
import org.ss.engine.phys.PhysicsObject;
import org.ss.engine.phys.PhysicsEngine;
import org.ss.game.object.SkyboxObject;

// Class that stores everything pertinent to a game scene
public class Scene {

    public PhysicsEngine physicsEngine;

    private PhysicsObject[] gameObjects;

    private SkyboxObject skyBox;

    private SceneLight sceneLight;

    // Constructor for the Scene class
    public Scene(){
        physicsEngine = new PhysicsEngine();
        sceneLight = new SceneLight();
    }

    // Getter for the GameObject collection from the physics engine
    public PhysicsObject[] getGameObjectArr() {
        return physicsEngine.getGameObjectArr();
    }

    // Getter for the ParticleObject collection from the physics engine
    public PhysicsObject[] getParticleObjectArr() {
        return physicsEngine.getParticleObjectArr();
    }

    // Getter for the ProjectileObject collection from the physics engine
    public PhysicsObject[] getProjectileObjectArr() {
        return physicsEngine.getProjectileObjectArr();
    }

    // Setter for the GameObject collection attribute
    public void setGameObjects(PhysicsObject[] gameObjects) {
        this.gameObjects = gameObjects;
    }

    // Getter for the SkyBox object attribute
    public SkyboxObject getSkyBox() {
        return skyBox;
    }

    // Setter for the SkyBox object attribute
    public void setSkyBox(SkyboxObject skyBox) {
        this.skyBox = skyBox;
    }

    // Getter for the SceneLight object attribute
    public SceneLight getSceneLight() {
        return sceneLight;
    }

    // Setter for the SceneLight attribute
    public void setSceneLight(SceneLight sceneLight) {
        this.sceneLight = sceneLight;
    }
}
