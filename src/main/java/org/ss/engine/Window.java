package org.ss.engine;

import static org.lwjgl.glfw.GLFW.*;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;

// Class that implements the GLFW window methods
public class Window {

    private final String title;

    private int width;

    private int height;

    private long windowHandle;

    private boolean resized;

    private boolean vSync;

    // Constructor for the GLFW Window Class
    public Window(String title, int width, int height, boolean vSync) {
        this.title = title;
        this.width = width;
        this.height = height;
        this.vSync = vSync;
        this.resized = false;
    }

    // Initializes the Window instance
    public void init() {
        // Setup error callback
        GLFWErrorCallback.createPrint(System.err).set();

        // Initialize GLFW
        if (!glfwInit()) {
            throw new IllegalStateException("Unable to initialize GLFW");
        }

        // Setup window hints
        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GL_TRUE);
        glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

        // Create the window
        windowHandle = glfwCreateWindow(width, height, title, NULL, NULL);
        if (windowHandle == NULL) {
            throw new RuntimeException("Failed to create the GLFW window");
        }

        // Setup resize callback
        glfwSetFramebufferSizeCallback(windowHandle, (window, width, height) -> {
           this.width = width;
           this.height = height;
           this.setResized(true);
        });

        // Setup escape key callback
        glfwSetKeyCallback(windowHandle, (window, key, scancode, action, mods) -> {
            if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
                glfwSetWindowShouldClose(window, true); // Detected in the rendering loop
            }
        });

        // Get primary monitor resolution and center window
        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        glfwSetWindowPos(
                windowHandle,
                (vidmode.width() - width) / 2,
                (vidmode.height() - height) / 2
        );

        // Make the OpenGL context current
        glfwMakeContextCurrent(windowHandle);

        if (isvSync()) {
            // Enable v-sync
            glfwSwapInterval(1);
        }

        // Make the window visible
        glfwShowWindow(windowHandle);

        GL.createCapabilities();

        // Set the clear color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glEnable(GL_DEPTH_TEST);
    }

    // Clears the set window color
    public void setClearColor(float r, float g, float b, float alpha) {
        glClearColor(r, g, b, alpha);
    }

    // Returns true if a key is pressed
    public boolean isKeyPressed(int keyCode) {
        return glfwGetKey(windowHandle, keyCode) == GLFW_PRESS;
    }

    // Returns true if the window is flagged to close
    public boolean windowShouldClose() {
        return glfwWindowShouldClose(windowHandle);
    }

    // Getter for the title attribute
    public String getTitle() {
        return title;
    }

    // Getter for the width attribute
    public int getWidth() {
        return width;
    }

    // Getter for the height attribute
    public int getHeight() {
        return height;
    }

    // Returns true if the window has been resized
    public boolean isResized() {
        return resized;
    }

    // Setter for the resized attribute
    public void setResized(boolean resized) {
        this.resized = resized;
    }

    // Getter for the vSync boolean attribute
    public boolean isvSync() {
        return vSync;
    }

    // Setter for the vSync boolean attribute
    public void setvSync(boolean vSync) {
        this.vSync = vSync;
    }

    // Updates the graphics displayed in the GLFW window
    public void update() {
        glfwSwapBuffers(windowHandle);
        glfwPollEvents();
    }

    // Getter for the window handle
    public long getWindowHandle() {
        return windowHandle;
    }
}
