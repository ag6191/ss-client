package org.ss.stub;

import org.joml.Vector3f;
import org.lwjgl.system.CallbackI;
import org.ss.engine.Timer;
import org.ss.engine.phys.ColliderType;
import org.ss.engine.phys.PhysicsObject;
import org.ss.game.object.*;

public class TestStubLoader {

    public static PhysicsObject getPhysicsObject() {
        return new PhysicsObject(null, 1, new Vector3f(), ColliderType.NONE);
    }

    public static boolean physicsObjectInArray(PhysicsObject physicsObject, PhysicsObject[] physicsObjectArr) {
        for (PhysicsObject otherPhysicsObject : physicsObjectArr) {
            if (physicsObject == otherPhysicsObject) { return true; }
        }
        return false;
    }

    public static GameObject getGameObject() {
        return new GameObject(null, 1, new Vector3f());
    }

    public static GunObject getGunObject(float damage, float range, float velocity) {
        return new GunObject(damage, range, velocity, true);
    }

    public static ParticleObject getParticleObject(float duration) {
        return new ParticleObject(null, 1, new Vector3f(), duration, Timer.getTime());
    }

    public static ProjectileObject getProjectileObject(float range, float damage) {
        return new ProjectileObject(null, 1, new Vector3f(), range, damage);
    }

    public static ShipObject getShipObject() throws Exception {
        return new ShipObject(null, 1, new Vector3f());
    }
}
