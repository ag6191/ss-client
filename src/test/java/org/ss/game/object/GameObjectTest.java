package org.ss.game.object;

import org.junit.jupiter.api.Test;
import org.ss.stub.TestStubLoader;

import static org.junit.jupiter.api.Assertions.*;

class GameObjectTest {

    @Test
    void getHealth() {
        GameObject testGameObject = TestStubLoader.getGameObject();
        assertEquals(100.0f, testGameObject.getHealth());
    }

    @Test
    void setHealth() {
        GameObject testGameObject = TestStubLoader.getGameObject();
        testGameObject.setHealth(1000.0f);
        assertEquals(1000.0f, testGameObject.getHealth());
    }

    @Test
    void reduceHealth() {
        GameObject testGameObject = TestStubLoader.getGameObject();
        testGameObject.reduceHealth(1.1f);
        assertEquals(98.9f, testGameObject.getHealth());
    }
}