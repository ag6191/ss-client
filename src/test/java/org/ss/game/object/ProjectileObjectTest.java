package org.ss.game.object;

import org.junit.jupiter.api.Test;
import org.ss.stub.TestStubLoader;

import static org.junit.jupiter.api.Assertions.*;

class ProjectileObjectTest {

    @Test
    void outOfRange() {
        ProjectileObject testProjectileObject = TestStubLoader.getProjectileObject(10, 10);
        assertEquals(false, testProjectileObject.outOfRange());

        testProjectileObject.setPosition(5, 0, 0);
        assertEquals(false, testProjectileObject.outOfRange());

        testProjectileObject.setPosition(11, 0, 0);
        assertEquals(true, testProjectileObject.outOfRange());

        testProjectileObject.setPosition(9, 9, 9);
        assertEquals(true, testProjectileObject.outOfRange());
    }

    @Test
    void getOriginPosition() {
        ProjectileObject testProjectileObject = TestStubLoader.getProjectileObject(10, 10);
        assertTrue(testProjectileObject.getOriginPosition().equals(0, 0, 0));
    }

    @Test
    void getRange() {
        ProjectileObject testProjectileObject = TestStubLoader.getProjectileObject(10, 10);
        assertEquals(10.0f, testProjectileObject.getRange());
    }

    @Test
    void getDamage() {
        ProjectileObject testProjectileObject = TestStubLoader.getProjectileObject(10, 10);
        assertEquals(10.0f, testProjectileObject.getDamage());
    }
}