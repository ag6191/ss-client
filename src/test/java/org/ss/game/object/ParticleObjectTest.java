package org.ss.game.object;

import org.junit.jupiter.api.Test;
import org.ss.stub.TestStubLoader;

import static org.junit.jupiter.api.Assertions.*;

class ParticleObjectTest {

    @Test
    void canDeleteParticle() throws InterruptedException {
        ParticleObject testParticleObject = TestStubLoader.getParticleObject(0.5f);
        assertEquals(false, testParticleObject.canDeleteParticle());
        Thread.sleep(501);
        assertEquals(true, testParticleObject.canDeleteParticle());
    }
}