package org.ss.game.object;

import org.joml.Vector3f;
import org.junit.jupiter.api.Test;
import org.ss.stub.TestStubLoader;

import static org.junit.jupiter.api.Assertions.*;

class GunObjectTest {

    @Test
    void fireGun() {
        GunObject testGunObject = TestStubLoader.getGunObject(10, 10, 10);
        ProjectileObject testProjectileObject = testGunObject.fireGun(new Vector3f(), new Vector3f(10, 10, 10), new Vector3f());
        assertTrue(null != testProjectileObject);
    }
}