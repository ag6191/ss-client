package org.ss.engine.graph;

import org.joml.Vector3f;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.ss.engine.Utils;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import static org.junit.jupiter.api.Assertions.*;

class CameraTest {
    private Camera camera;

    private Vector3f position;

    private Vector3f rotation;

    @BeforeEach
    void setUp() {
        float x,y,z,a,b,c;
        x = Utils.randFloat(0.0f,1.0f);
        y = Utils.randFloat(0.0f,1.0f);
        z = Utils.randFloat(0.0f,1.0f);
        a = Utils.randFloat(0.0f,1.0f);
        b = Utils.randFloat(0.0f,1.0f);
        c = Utils.randFloat(0.0f,1.0f);
        Vector3f position = new Vector3f(x,y,z);
        Vector3f rotation = new Vector3f(a,b,c);
        this.camera = new Camera(position,rotation);
    }

    @Test
    void movePosition() {
        //Test function when z == 0 && x != 0
        float originalX = this.camera.getPosition().x;
        float originalY = this.camera.getPosition().y;
        float originalZ = this.camera.getPosition().z;
        float yRotation = this.camera.getRotation().y;
        float x,y,z;
        x = Utils.randFloat(0.0f,1.0f);
        y = Utils.randFloat(0.0f,1.0f);
        z = 0.0f;
        this.camera.movePosition(x,y,z);
        float sinOffsetX = (float) (Math.sin(Math.toRadians(yRotation - 90))* -1.0 * x) ;
        float cosOffsetX = (float) Math.cos(Math.toRadians(yRotation - 90)) * x;
        //Calculate difference between adding up offset and original and using move position.
        //If it is less than .00001 then the two values are virtually identical and the move position method works.
        boolean virt_equal0 = Math.abs(originalX + sinOffsetX - this.camera.getPosition().x) < 0.00001;
        boolean virt_equal1 = Math.abs(originalZ + cosOffsetX - this.camera.getPosition().z) < 0.00001;
        boolean virt_equal2 = Math.abs(originalY+ y - this.camera.getPosition().y) < 0.00001;
        assertTrue(virt_equal0);
        assertTrue(virt_equal1);
        assertTrue(virt_equal2);



        //OffsetZ != 0 && OffsetX == 0
        originalX = this.camera.getPosition().x;
        originalY = this.camera.getPosition().y;
        originalZ = this.camera.getPosition().z;
        yRotation = this.camera.getRotation().y;
        x = 0.0f;
        y = Utils.randFloat(0.0f,1.0f);
        z = Utils.randFloat(0.0f,1.0f);
        this.camera.movePosition(x,y,z);


        float sinOffsetZ = (float) (Math.sin(Math.toRadians(yRotation))* -1.0 * z) ;
        float cosOffsetZ = (float) Math.cos(Math.toRadians(yRotation)) * z;
        virt_equal0 = Math.abs(originalX + sinOffsetZ - this.camera.getPosition().x) < 0.00001;
        virt_equal1 = Math.abs(originalZ + cosOffsetZ - this.camera.getPosition().z) < 0.00001;
        virt_equal2 = Math.abs(originalY+ y - this.camera.getPosition().y) < 0.00001;
        assertTrue(virt_equal0);
        assertTrue(virt_equal1);
        assertTrue(virt_equal2);

        //OffsetZ == 0 && OffsetX == 0
        originalY = this.camera.getPosition().y;
        x = 0.0f;
        y = Utils.randFloat(0.0f,1.0f);
        z = 0.0f;
        this.camera.movePosition(x,y,z);
        virt_equal0 = Math.abs(originalY+ y - this.camera.getPosition().y) < 0.00001;
        assertTrue(virt_equal0);
    }

    @Test
    void moveRotation() {
        float x,y,z, originalX, originalY,originalZ;
        x = Utils.randFloat(0.0f,1.0f);
        y = Utils.randFloat(0.0f,1.0f);
        z = Utils.randFloat(0.0f,1.0f);
        originalX = this.camera.getRotation().x;
        originalY =this.camera.getRotation().y;
        originalZ = this.camera.getRotation().z;
        this.camera.moveRotation(x,y,z);

        boolean virt_equal0 = Math.abs(originalX + x - this.camera.getRotation().x) < 0.00001;
        boolean virt_equal1 = Math.abs(originalZ + z - this.camera.getRotation().z) < 0.00001;
        boolean virt_equal2 = Math.abs(originalY+ y - this.camera.getRotation().y) < 0.00001;

        assertTrue(virt_equal0 && virt_equal1 && virt_equal2);



    }
}