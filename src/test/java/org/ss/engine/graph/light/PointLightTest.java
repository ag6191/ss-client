package org.ss.engine.graph.light;

import org.joml.Vector3f;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.ss.engine.Utils;

import static org.junit.jupiter.api.Assertions.*;

class PointLightTest {
    private PointLight ptLight;

    private Vector3f color;

    private Vector3f position;

    protected float intensity;

    private PointLight.Attenuation attenuation;

    @BeforeEach
    void setUp() {
        float x,y,z,a,b,c,i,cnst,lin,exp;
        x = Utils.randFloat(0.0f,1.0f);
        y = Utils.randFloat(0.0f,1.0f);
        z = Utils.randFloat(0.0f,1.0f);
        a = Utils.randFloat(0.0f,1.0f);
        b = Utils.randFloat(0.0f,1.0f);
        c = Utils.randFloat(0.0f,1.0f);
        i = Utils.randFloat(0.0f,1.0f);
        cnst = Utils.randFloat(0.0f,1.0f);
        lin = Utils.randFloat(0.0f,1.0f);
        exp = Utils.randFloat(0.0f,1.0f);
        Vector3f color = new Vector3f(x,y,z);
        Vector3f position = new Vector3f(a,b,c);
        PointLight.Attenuation attenuation = new PointLight.Attenuation(cnst, lin, exp);
        this.color = color;
        this.position = position;
        this.intensity = i;
        this.attenuation = attenuation;
        this.ptLight = new PointLight(color, position, i, attenuation);
    }

    @Test
    void getColor() {
        Vector3f test1 = new Vector3f(1.0001f,1.0001f,1.0001f);
        Vector3f test2 = new Vector3f(1.000001f,1.000001f,1.000001f);
        Vector3f object = this.ptLight.getColor();

        assertEquals(this.color, object);
        assertTrue(this.color.equals(object));

        assertNotEquals(test1, object);
        assertFalse(test1.equals(object));

        assertNotEquals(test2, object);
        assertFalse(test2.equals(object));
    }

    @Test
    void setColor() {
        Vector3f test1 = new Vector3f(1.0001f,1.0001f,1.0001f);
        Vector3f test2 = new Vector3f(1.000001f,1.000001f,1.000001f);
        ptLight.setColor(test1);
        Vector3f object = ptLight.getColor();

        assertEquals(test1, object);
        assertTrue(test1.equals(object));

        assertNotEquals(this.color,object);
        assertFalse(this.color.equals(object));

        assertNotEquals(test2,object);
        assertFalse(test2.equals(object));
    }

    @Test
    void getPosition() {
        Vector3f test1 = new Vector3f(1.0001f,1.0001f,1.0001f);
        Vector3f test2 = new Vector3f(1.000001f,1.000001f,1.000001f);
        Vector3f object = this.ptLight.getPosition();

        assertEquals(this.position, object);
        assertTrue(this.position.equals(object));

        assertNotEquals(test1, object);
        assertFalse(test1.equals(object));

        assertNotEquals(test2, object);
        assertFalse(test2.equals(object));
    }

    @Test
    void setPosition() {
        Vector3f test1 = new Vector3f(1.0001f,1.0001f,1.0001f);
        Vector3f test2 = new Vector3f(1.000001f,1.000001f,1.000001f);
        ptLight.setPosition(test1);
        Vector3f object = ptLight.getPosition();


        assertEquals(test1, object);
        assertTrue(test1.equals(object));

        assertNotEquals(this.position,object);
        assertFalse(this.position.equals(object));

        assertNotEquals(test2,object);
        assertFalse(test2.equals(object));
    }

    @Test
    void getIntensity() {
        float test1 = 1.0001f;
        float test2 = 1.000001f;
        float object = this.ptLight.getIntensity();

        assertEquals(this.intensity, object);
        assertTrue(this.intensity == object);

        assertNotEquals(test1, object);
        assertFalse(test1 == object);

        assertNotEquals(test2, object);
        assertFalse(test2 == object);
    }

    @Test
    void setIntensity() {
        float test1 = 1.0001f;
        float test2 = 1.000001f;
        ptLight.setIntensity(test1);
        float object = ptLight.getIntensity();


        assertEquals(test1, object);
        assertTrue(test1 == object);

        assertNotEquals(this.intensity, object);
        assertFalse(this.intensity == object);

        assertNotEquals(test2, object);
        assertFalse(test2 == object);
    }

    @Test
    void getAttenuation() {
        PointLight.Attenuation test1 = new PointLight.Attenuation(1.0001f,1.0001f,1.0001f);
        PointLight.Attenuation test2 = new PointLight.Attenuation(1.000001f,1.000001f,1.000001f);
        PointLight.Attenuation object = this.ptLight.getAttenuation();

        assertEquals(this.attenuation, object);
        assertTrue(this.attenuation == object);

        assertNotEquals(test1, object);
        assertFalse(test1 == object);

        assertNotEquals(test2, object);
        assertFalse(test2 == object);


    }

    @Test
    void setAttenuation() {
        PointLight.Attenuation test1 = new PointLight.Attenuation(1.0001f,1.0001f,1.0001f);
        PointLight.Attenuation test2 = new PointLight.Attenuation(1.000001f,1.000001f,1.000001f);
        this.ptLight.setAttenuation(test1);
        PointLight.Attenuation object = this.ptLight.getAttenuation();

        assertEquals(test1, object);
        assertTrue(test1 == object);

        assertNotEquals(this.attenuation, object);
        assertFalse(this.attenuation == object);

        assertNotEquals(test2, object);
        assertFalse(test2 == object);
    }
}