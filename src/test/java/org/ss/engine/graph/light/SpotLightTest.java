package org.ss.engine.graph.light;

import org.joml.Vector3f;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.ss.engine.Utils;

import static org.junit.jupiter.api.Assertions.*;

class SpotLightTest {

    private SpotLight spotLight;

    private PointLight pointLight;

    private Vector3f coneDirection;

    private float cutOff;

    @BeforeEach
    void setUp() {
        float x,y,z,cut;
        x = Utils.randFloat(0.0f,1.0f);
        y = Utils.randFloat(0.0f,1.0f);
        z = Utils.randFloat(0.0f,1.0f);
        cut = Utils.randFloat(0.0f,1.0f);
        Vector3f coneDirection = new Vector3f(x,y,z);
        this.cutOff = cut;
        this.coneDirection = coneDirection;
        this.pointLight = makePointLight();
        this.spotLight = new SpotLight(this.pointLight, coneDirection, cut);
    }

    private PointLight makePointLight() {
        float x,y,z,a,b,c,i,cnst,lin,exp;
        x = Utils.randFloat(0.0f,1.0f);
        y = Utils.randFloat(0.0f,1.0f);
        z = Utils.randFloat(0.0f,1.0f);
        a = Utils.randFloat(0.0f,1.0f);
        b = Utils.randFloat(0.0f,1.0f);
        c = Utils.randFloat(0.0f,1.0f);
        i = Utils.randFloat(0.0f,1.0f);
        cnst = Utils.randFloat(0.0f,1.0f);
        lin = Utils.randFloat(0.0f,1.0f);
        exp = Utils.randFloat(0.0f,1.0f);
        Vector3f color = new Vector3f(x,y,z);
        Vector3f position = new Vector3f(a,b,c);
        PointLight.Attenuation attenuation = new PointLight.Attenuation(cnst, lin, exp);
        return new PointLight(color, position, i, attenuation);
    }

    @Test
    void getPointLight() {

        PointLight test1 = makePointLight();
        PointLight test2 = makePointLight();
        PointLight object = this.spotLight.getPointLight();

        assertEquals(this.pointLight, object);
        assertTrue(this.pointLight == object);

        assertNotEquals(test1, object);
        assertFalse(test1 == object);

        assertNotEquals(test2, object);
        assertFalse(test2 == object);

    }

    @Test
    void setPointLight() {
        PointLight test1 = makePointLight();
        PointLight test2 = makePointLight();
        this.spotLight.setPointLight(test1);
        PointLight object = this.spotLight.getPointLight();

        assertEquals(test1, object);
        assertTrue(test1 == object);

        assertNotEquals(this.pointLight, object);
        assertFalse(this.pointLight == object);

        assertNotEquals(test2, object);
        assertFalse(test2 == object);
    }

    @Test
    void getConeDirection() {
        Vector3f test1 = new Vector3f(1.0001f,1.0001f,1.0001f);
        Vector3f test2 = new Vector3f(1.000001f,1.000001f,1.000001f);
        Vector3f object = this.spotLight.getConeDirection();

        assertEquals(this.coneDirection, object);
        assertTrue(this.coneDirection.equals(object));

        assertNotEquals(test1,object);
        assertFalse(test1.equals(object));

        assertNotEquals(test2,object);
        assertFalse(test2.equals(object));

    }

    @Test
    void setConeDirection() {
        Vector3f test1 = new Vector3f(1.0001f,1.0001f,1.0001f);
        Vector3f test2 = new Vector3f(1.000001f,1.000001f,1.000001f);
        this.spotLight.setConeDirection(test1);
        Vector3f object = this.spotLight.getConeDirection();

        assertEquals(test1, object);
        assertTrue(test1.equals(object));

        assertNotEquals(this.coneDirection,object);
        assertFalse(this.coneDirection.equals(object));

        assertNotEquals(test2,object);
        assertFalse(test2.equals(object));
    }

    @Test
    void getCutOff() {
        float test1 = 1.0001f;
        float test2 = 1.000001f;
        float object = this.spotLight.getCutOff();

        assertEquals((float) Math.cos(Math.toRadians(this.cutOff)), object);
        assertTrue((float) Math.cos(Math.toRadians(this.cutOff)) == object);

        assertNotEquals(test1, object);
        assertFalse(test1 == object);

        assertNotEquals(test2, object);
        assertFalse(test2 == object);
    }

    @Test
    void setCutOff() {
        float test1 = 1.0001f;
        float test2 = 1.000001f;
        this.spotLight.setCutOff(test1);
        float object = this.spotLight.getCutOff();

        assertEquals(test1, object);
        assertTrue(test1 == object);

        assertNotEquals(this.cutOff, object);
        assertFalse(this.cutOff == object);

        assertNotEquals(test2, object);
        assertFalse(test2 == object);
    }

    @Test
    void setCutOffAngle() {
        float test1 = (float) Math.cos(Math.toRadians(1.6f));
        float test2 = (float) Math.cos(Math.toRadians(1.0001f));
        this.spotLight.setCutOff(test1);
        float object = this.spotLight.getCutOff();

        assertEquals(test1, object);
        assertTrue(test1 == object);

        assertNotEquals(this.cutOff, object);
        assertFalse(this.cutOff == object);

        assertNotEquals(test2, object);
        assertFalse(test2 == object);
    }
}