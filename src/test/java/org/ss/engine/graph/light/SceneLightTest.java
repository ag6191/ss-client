package org.ss.engine.graph.light;

import org.ss.engine.Utils;
import org.ss.engine.loader.LightLoader;
import org.joml.Vector3f;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SceneLightTest {

    private SceneLight sceneLight;

    private Vector3f ambientLight;

    private PointLight[] pointLightList;

    private SpotLight[] spotLightList;

    private DirectionalLight directionalLight;

    @BeforeEach
    void setUp(){
        this.ambientLight = new Vector3f(0.3f, 0.3f, 0.3f);
        this.pointLightList = new PointLight[]{makePointLight()};
        this.spotLightList = new SpotLight[]{makeSpotLight()};
        this.directionalLight = makeDirectionalLight();
        this.sceneLight = new SceneLight(this.ambientLight, this.pointLightList, this.spotLightList, this.directionalLight);

    }

    private PointLight makePointLight() {
        float x,y,z,a,b,c,i,cnst,lin,exp;
        x = Utils.randFloat(0.0f,1.0f);
        y = Utils.randFloat(0.0f,1.0f);
        z = Utils.randFloat(0.0f,1.0f);
        a = Utils.randFloat(0.0f,1.0f);
        b = Utils.randFloat(0.0f,1.0f);
        c = Utils.randFloat(0.0f,1.0f);
        i = Utils.randFloat(0.0f,1.0f);
        cnst = Utils.randFloat(0.0f,1.0f);
        lin = Utils.randFloat(0.0f,1.0f);
        exp = Utils.randFloat(0.0f,1.0f);
        Vector3f color = new Vector3f(x,y,z);
        Vector3f position = new Vector3f(a,b,c);
        PointLight.Attenuation attenuation = new PointLight.Attenuation(cnst, lin, exp);
        return new PointLight(color, position, i, attenuation);
    }

    private SpotLight makeSpotLight(){
        float x,y,z,cut;
        x = Utils.randFloat(0.0f,1.0f);
        y = Utils.randFloat(0.0f,1.0f);
        z = Utils.randFloat(0.0f,1.0f);
        cut = Utils.randFloat(0.0f,1.0f);
        Vector3f coneDirection = new Vector3f(x,y,z);
        return new SpotLight(makePointLight(), coneDirection, cut);
    }

    private DirectionalLight makeDirectionalLight(){
        float x,y,z,a,b,c,i;
        x = Utils.randFloat(0.0f,1.0f);
        y = Utils.randFloat(0.0f,1.0f);
        z = Utils.randFloat(0.0f,1.0f);
        a = Utils.randFloat(0.0f,1.0f);
        b = Utils.randFloat(0.0f,1.0f);
        c = Utils.randFloat(0.0f,1.0f);
        i = Utils.randFloat(0.0f,1.0f);
        Vector3f color = new Vector3f(x,y,z);
        Vector3f direction = new Vector3f(a,b,c);
        return new DirectionalLight(color, direction, i);

    }

    @Test
    void GetAmbientLight() {
        Vector3f test1 = new Vector3f(1.0001f,1.0001f,1.0001f);
        Vector3f test2 = new Vector3f(1.000001f,1.000001f,1.000001f);
        Vector3f object = this.sceneLight.getAmbientLight();

        assertEquals(this.ambientLight, object);
        assertTrue(this.ambientLight.equals(object));

        assertNotEquals(test1, object);
        assertFalse(test1.equals(object));

        assertNotEquals(test2, object);
        assertFalse(test2.equals(object));
    }

    @Test
    void SetAmbientLight() {
        Vector3f test1 = new Vector3f(1.0001f,1.0001f,1.0001f);
        Vector3f test2 = new Vector3f(1.000001f,1.000001f,1.000001f);
        this.sceneLight.setAmbientLight(test1);
        Vector3f object = this.sceneLight.getAmbientLight();

        assertEquals(test1, object);
        assertTrue(test1.equals(object));

        assertNotEquals(this.ambientLight, object);
        assertFalse(this.ambientLight.equals(object));

        assertNotEquals(test2, object);
        assertFalse(test2.equals(object));
    }

    @Test
    void GetPointLightList() {
        PointLight[] test1 = new PointLight[]{makePointLight()};
        PointLight[] test2 = new PointLight[]{makePointLight()};
        PointLight[] object = this.sceneLight.getPointLightList();

        assertEquals(this.pointLightList, object);
        assertTrue(this.pointLightList.equals(object));

        assertNotEquals(test1, object);
        assertFalse(test1.equals(object));

        assertNotEquals(test2, object);
        assertFalse(test2.equals(object));
    }

    @Test
    void SetPointLightList() {
        PointLight[] test1 = new PointLight[]{makePointLight()};
        PointLight[] test2 = new PointLight[]{makePointLight()};
        this.sceneLight.setPointLightList(test1);
        PointLight[] object = this.sceneLight.getPointLightList();

        assertEquals(test1, object);
        assertTrue(test1.equals(object));

        assertNotEquals(this.pointLightList, object);
        assertFalse(this.pointLightList.equals(object));

        assertNotEquals(test2, object);
        assertFalse(test2.equals(object));
    }

    @Test
    void GetSpotLightList() {
        SpotLight[] test1 = new SpotLight[]{makeSpotLight()};
        SpotLight[] test2 = new SpotLight[]{makeSpotLight()};
        SpotLight[] object = this.sceneLight.getSpotLightList();

        assertEquals(this.spotLightList, object);
        assertTrue(this.spotLightList.equals(object));

        assertNotEquals(test1, object);
        assertFalse(test1.equals(object));

        assertNotEquals(test2, object);
        assertFalse(test2.equals(object));
    }

    @Test
    void SetSpotLightList() {
        SpotLight[] test1 = new SpotLight[]{makeSpotLight()};
        SpotLight[] test2 = new SpotLight[]{makeSpotLight()};
        this.sceneLight.setSpotLightList(test1);
        SpotLight[] object = this.sceneLight.getSpotLightList();

        assertEquals(test1, object);
        assertTrue(test1.equals(object));

        assertNotEquals(this.spotLightList, object);
        assertFalse(this.spotLightList.equals(object));

        assertNotEquals(test2, object);
        assertFalse(test2.equals(object));
    }

    @Test
    void GetDirectionalLight() {
        DirectionalLight test1 = makeDirectionalLight();
        DirectionalLight test2 = makeDirectionalLight();
        DirectionalLight object = this.sceneLight.getDirectionalLight();

        assertEquals(this.directionalLight, object);
        assertTrue(this.directionalLight.equals(object));

        assertNotEquals(test1, object);
        assertFalse(test1.equals(object));

        assertNotEquals(test2, object);
        assertFalse(test2.equals(object));
    }

    @Test
    void SetDirectionalLight() {
        DirectionalLight test1 = makeDirectionalLight();
        DirectionalLight test2 = makeDirectionalLight();
        this.sceneLight.setDirectionalLight(test1);
        DirectionalLight object = this.sceneLight.getDirectionalLight();

        assertEquals(test1, object);
        assertTrue(test1.equals(object));

        assertNotEquals(this.directionalLight, object);
        assertFalse(this.directionalLight.equals(object));

        assertNotEquals(test2, object);
        assertFalse(test2.equals(object));
    }
}