package org.ss.engine.graph.light;

import org.joml.Vector3f;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.ss.engine.Utils;

import static org.junit.jupiter.api.Assertions.*;

class DirectionalLightTest {
    private DirectionalLight dirLight;

    private Vector3f color;

    private Vector3f direction;

    private float intensity;

    @BeforeEach
    void setUp() {
        float x,y,z,a,b,c,i;
        x = Utils.randFloat(0.0f,1.0f);
        y = Utils.randFloat(0.0f,1.0f);
        z = Utils.randFloat(0.0f,1.0f);
        a = Utils.randFloat(0.0f,1.0f);
        b = Utils.randFloat(0.0f,1.0f);
        c = Utils.randFloat(0.0f,1.0f);
        i = Utils.randFloat(0.0f,1.0f);
        Vector3f color = new Vector3f(x,y,z);
        Vector3f direction = new Vector3f(a,b,c);
        this.color = color;
        this.direction = direction;
        this.intensity = i;
        this.dirLight = new DirectionalLight(color, direction, i);
    }

    @Test
    void getColor() {
        Vector3f test1 = new Vector3f(1.0001f,1.0001f,1.0001f);
        Vector3f test2 = new Vector3f(1.000001f,1.000001f,1.000001f);
        Vector3f object = this.dirLight.getColor();

        assertEquals(this.color, object);
        assertTrue(this.color.equals(object));

        assertNotEquals(test1, object);
        assertFalse(test1.equals(object));

        assertNotEquals(test2, object);
        assertFalse(test2.equals(object));
    }

    @Test
    void setColor() {
        Vector3f test1 = new Vector3f(1.0001f,1.0001f,1.0001f);
        Vector3f test2 = new Vector3f(1.000001f,1.000001f,1.000001f);
        dirLight.setColor(test1);
        Vector3f object = dirLight.getColor();

        assertEquals(test1, object);
        assertTrue(test1.equals(object));

        assertNotEquals(this.color,object);
        assertFalse(this.color.equals(object));

        assertNotEquals(test2,object);
        assertFalse(test2.equals(object));
    }

    @Test
    void getDirection() {
        Vector3f test1 = new Vector3f(1.0001f,1.0001f,1.0001f);
        Vector3f test2 = new Vector3f(1.000001f,1.000001f,1.000001f);
        Vector3f object = this.dirLight.getDirection();

        assertEquals(this.direction, object);
        assertTrue(this.direction.equals(object));

        assertNotEquals(test1, object);
        assertFalse(test1.equals(object));

        assertNotEquals(test2, object);
        assertFalse(test2.equals(object));
    }

    @Test
    void setDirection() {
        Vector3f test1 = new Vector3f(1.0001f,1.0001f,1.0001f);
        Vector3f test2 = new Vector3f(1.000001f,1.000001f,1.000001f);
        dirLight.setDirection(test1);
        Vector3f object = dirLight.getDirection();


        assertEquals(test1, object);
        assertTrue(test1.equals(object));

        assertNotEquals(this.direction,object);
        assertFalse(this.direction.equals(object));

        assertNotEquals(test2,object);
        assertFalse(test2.equals(object));
    }

    @Test
    void getIntensity() {
        float test1 = 1.0001f;
        float test2 = 1.000001f;
        float object = this.dirLight.getIntensity();

        assertEquals(this.intensity, object);
        assertTrue(this.intensity == object);

        assertNotEquals(test1, object);
        assertFalse(test1 == object);

        assertNotEquals(test2, object);
        assertFalse(test2 == object);
    }

    @Test
    void setIntensity() {
        float test1 = 1.0001f;
        float test2 = 1.000001f;
        dirLight.setIntensity(test1);
        float object = dirLight.getIntensity();


        assertEquals(test1, object);
        assertTrue(test1 == object);

        assertNotEquals(this.intensity, object);
        assertFalse(this.intensity == object);

        assertNotEquals(test2, object);
        assertFalse(test2 == object);
    }
}