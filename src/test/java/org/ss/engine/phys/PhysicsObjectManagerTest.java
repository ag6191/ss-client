package org.ss.engine.phys;

import static org.junit.jupiter.api.Assertions.*;

import org.joml.Vector3f;
import org.junit.jupiter.api.*;
import org.ss.stub.TestStubLoader;

import javax.rmi.CORBA.Stub;

class PhysicsObjectManagerTest {

    @Test
    void getFirst() {
        PhysicsObjectManager testManager = new PhysicsObjectManager();
        PhysicsObject firstObject = TestStubLoader.getPhysicsObject();
        PhysicsObject secondObject = TestStubLoader.getPhysicsObject();
        PhysicsObject thirdObject = TestStubLoader.getPhysicsObject();

        testManager.addFirst(firstObject);
        assertEquals(firstObject, testManager.getFirst());

        testManager.addLast(secondObject);
        assertEquals(firstObject, testManager.getFirst());

        testManager.addFirst(thirdObject);
        assertEquals(thirdObject, testManager.getFirst());

        testManager.removeLast();
        assertEquals(thirdObject, testManager.getFirst());

        testManager.removeFirst();
        assertEquals(firstObject, testManager.getFirst());
    }

    @Test
    void getLast() {
        PhysicsObjectManager testManager = new PhysicsObjectManager();
        PhysicsObject firstObject = TestStubLoader.getPhysicsObject();
        PhysicsObject secondObject = TestStubLoader.getPhysicsObject();
        PhysicsObject thirdObject = TestStubLoader.getPhysicsObject();

        testManager.addFirst(firstObject);
        assertEquals(firstObject, testManager.getLast());
        assertEquals(testManager.getFirst(), testManager.getLast());

        testManager.addLast(secondObject);
        assertEquals(secondObject, testManager.getLast());

        testManager.addFirst(thirdObject);
        assertEquals(thirdObject, testManager.getFirst());
        assertEquals(secondObject, testManager.getLast());

        testManager.removeLast();
        assertEquals(firstObject, testManager.getLast());

        testManager.removeLast();
        assertEquals(thirdObject, testManager.getFirst());
    }

    @Test
    void addLast() {
        PhysicsObjectManager testManager = new PhysicsObjectManager();
        PhysicsObject firstObject = TestStubLoader.getPhysicsObject();
        PhysicsObject secondObject = TestStubLoader.getPhysicsObject();
        PhysicsObject thirdObject = TestStubLoader.getPhysicsObject();

        testManager.addLast(firstObject);
        assertEquals(firstObject, testManager.getLast());

        testManager.addFirst(secondObject);
        assertEquals(firstObject, testManager.getLast());

        testManager.addLast(thirdObject);
        assertEquals(thirdObject, testManager.getLast());

        testManager.removeLast();
        assertEquals(firstObject, testManager.getLast());
    }

    @Test
    void addFirst() {
        PhysicsObjectManager testManager = new PhysicsObjectManager();
        PhysicsObject firstObject = TestStubLoader.getPhysicsObject();
        PhysicsObject secondObject = TestStubLoader.getPhysicsObject();
        PhysicsObject thirdObject = TestStubLoader.getPhysicsObject();

        testManager.addLast(firstObject);
        assertEquals(firstObject, testManager.getFirst());

        testManager.addFirst(secondObject);
        assertEquals(secondObject, testManager.getFirst());

        testManager.addLast(thirdObject);
        assertEquals(secondObject, testManager.getFirst());

        testManager.removeFirst();
        assertEquals(firstObject, testManager.getFirst());
    }

    @Test
    void remove() {
        PhysicsObjectManager testManager = new PhysicsObjectManager();
        PhysicsObject firstObject = TestStubLoader.getPhysicsObject();
        PhysicsObject secondObject = TestStubLoader.getPhysicsObject();
        PhysicsObject thirdObject = TestStubLoader.getPhysicsObject();

        testManager.addLast(firstObject);
        testManager.addLast(secondObject);
        assertEquals(2, testManager.size());

        testManager.remove(0);
        assertEquals(1, testManager.size());
        assertEquals(secondObject, testManager.getFirst());

        testManager.addFirst(thirdObject);
        testManager.removeLast();
        assertEquals(1, testManager.size());
        assertEquals(thirdObject, testManager.getLast());
    }

    @Test
    void staticRemove() {
        PhysicsObjectManager testManager = new PhysicsObjectManager();
        PhysicsObject firstObject = TestStubLoader.getPhysicsObject();
        PhysicsObject secondObject = TestStubLoader.getPhysicsObject();
        PhysicsObject thirdObject = TestStubLoader.getPhysicsObject();
        PhysicsObject fourthObject = TestStubLoader.getPhysicsObject();
        PhysicsObject fifthObject = TestStubLoader.getPhysicsObject();

        testManager.addFirst(firstObject);
        testManager.addFirst(secondObject);
        testManager.addLast(thirdObject);
        PhysicsObjectManager.remove(secondObject);
        assertEquals(2, testManager.size());

        testManager.addLast(fourthObject);
        testManager.addFirst(fifthObject);
        PhysicsObjectManager.remove(firstObject);
        PhysicsObject[] physicsObjectArr = testManager.toArray();
        assertEquals(3, testManager.size());
        assertEquals(3, physicsObjectArr.length);
        assertEquals(fifthObject, physicsObjectArr[0]);
        assertEquals(thirdObject, physicsObjectArr[1]);
        assertEquals(fourthObject, physicsObjectArr[2]);
    }

    @Test
    void toArray() {
        PhysicsObjectManager testManager = new PhysicsObjectManager();
        PhysicsObject firstObject = TestStubLoader.getPhysicsObject();
        PhysicsObject secondObject = TestStubLoader.getPhysicsObject();
        PhysicsObject thirdObject = TestStubLoader.getPhysicsObject();
        PhysicsObject fourthObject = TestStubLoader.getPhysicsObject();
        PhysicsObject fifthObject = TestStubLoader.getPhysicsObject();
        PhysicsObject sixthObject = TestStubLoader.getPhysicsObject();

        testManager.addFirst(firstObject);
        testManager.addFirst(secondObject);
        testManager.addLast(thirdObject);
        PhysicsObject[] physicsObjectArr = testManager.toArray();
        assertEquals(3, physicsObjectArr.length);
        assertEquals(firstObject, physicsObjectArr[1]);
        assertEquals(secondObject, physicsObjectArr[0]);
        assertEquals(thirdObject, physicsObjectArr[2]);

        testManager.removeFirst();
        testManager.addLast(fourthObject);
        testManager.addFirst(fifthObject);
        physicsObjectArr = testManager.toArray();
        assertEquals(4, physicsObjectArr.length);
        assertEquals(firstObject, physicsObjectArr[1]);
        assertEquals(fifthObject, physicsObjectArr[0]);
        assertEquals(thirdObject, physicsObjectArr[2]);
        assertEquals(fourthObject, physicsObjectArr[3]);
    }

    @Test
    void staticToArray() {
        PhysicsObjectManager testManager1 = new PhysicsObjectManager();
        PhysicsObjectManager testManager2 = new PhysicsObjectManager();
        PhysicsObject firstObject = TestStubLoader.getPhysicsObject();
        PhysicsObject secondObject = TestStubLoader.getPhysicsObject();
        PhysicsObject thirdObject = TestStubLoader.getPhysicsObject();
        PhysicsObject fourthObject = TestStubLoader.getPhysicsObject();
        PhysicsObject fifthObject = TestStubLoader.getPhysicsObject();
        PhysicsObject sixthObject = TestStubLoader.getPhysicsObject();

        testManager1.addLast(firstObject);
        testManager1.addLast(secondObject);
        testManager2.addLast(thirdObject);
        testManager2.addLast(fourthObject);

        PhysicsObject[] testArray = PhysicsObjectManager.toArray(
                new PhysicsObjectManager[]{testManager1, testManager2});
        assertEquals(4, testArray.length);
        assertTrue(TestStubLoader.physicsObjectInArray(firstObject, testArray));
        assertTrue(TestStubLoader.physicsObjectInArray(secondObject, testArray));
        assertTrue(TestStubLoader.physicsObjectInArray(thirdObject, testArray));
        assertTrue(TestStubLoader.physicsObjectInArray(fourthObject, testArray));

        testManager1.clear();
        testArray = PhysicsObjectManager.toArray(
                new PhysicsObjectManager[]{testManager1, testManager2});
        assertEquals(2, testArray.length);
        assertFalse(TestStubLoader.physicsObjectInArray(firstObject, testArray));
        assertFalse(TestStubLoader.physicsObjectInArray(secondObject, testArray));
        assertTrue(TestStubLoader.physicsObjectInArray(thirdObject, testArray));
        assertTrue(TestStubLoader.physicsObjectInArray(fourthObject, testArray));

        testManager1.addLast(fifthObject);
        testManager2.addLast(sixthObject);
        testArray = PhysicsObjectManager.toArray(
                new PhysicsObjectManager[]{testManager1, testManager2});
        assertEquals(4, testArray.length);
        assertTrue(TestStubLoader.physicsObjectInArray(thirdObject, testArray));
        assertTrue(TestStubLoader.physicsObjectInArray(fourthObject, testArray));
        assertTrue(TestStubLoader.physicsObjectInArray(fifthObject, testArray));
        assertTrue(TestStubLoader.physicsObjectInArray(sixthObject, testArray));
    }

    @Test
    void clear() {
        PhysicsObjectManager testManager = new PhysicsObjectManager();
        PhysicsObject firstObject = TestStubLoader.getPhysicsObject();
        PhysicsObject secondObject = TestStubLoader.getPhysicsObject();
        PhysicsObject thirdObject = TestStubLoader.getPhysicsObject();
        PhysicsObject fourthObject = TestStubLoader.getPhysicsObject();

        testManager.addFirst(firstObject);
        testManager.addFirst(secondObject);
        testManager.clear();
        assertEquals(0, testManager.size());
        assertFalse(testManager.contains(firstObject));
        assertFalse(testManager.contains(secondObject));

        testManager.addFirst(thirdObject);
        testManager.addFirst(fourthObject);
        assertTrue(testManager.contains(thirdObject));
        assertTrue(testManager.contains(fourthObject));
        assertEquals(2, testManager.size());

        testManager.clear();
        assertEquals(0, testManager.size());
        assertFalse(testManager.contains(thirdObject));
        assertFalse(testManager.contains(fourthObject));
    }

    @Test
    void contains() {
        PhysicsObjectManager testManager = new PhysicsObjectManager();
        PhysicsObject firstObject = TestStubLoader.getPhysicsObject();
        PhysicsObject secondObject = TestStubLoader.getPhysicsObject();
        PhysicsObject thirdObject = TestStubLoader.getPhysicsObject();

        assertFalse(testManager.contains(firstObject));
        assertFalse(testManager.contains(secondObject));
        assertFalse(testManager.contains(thirdObject));

        testManager.addFirst(firstObject);
        assertTrue(testManager.contains(firstObject));
        assertFalse(testManager.contains(secondObject));
        assertFalse(testManager.contains(thirdObject));

        testManager.addLast(secondObject);
        assertTrue(testManager.contains(firstObject));
        assertTrue(testManager.contains(secondObject));
        assertFalse(testManager.contains(thirdObject));

        testManager.addFirst(thirdObject);
        assertTrue(testManager.contains(firstObject));
        assertTrue(testManager.contains(secondObject));
        assertTrue(testManager.contains(thirdObject));

        testManager.removeFirst();
        assertTrue(testManager.contains(firstObject));
        assertTrue(testManager.contains(secondObject));
        assertFalse(testManager.contains(thirdObject));

        testManager.removeFirst();
        assertFalse(testManager.contains(firstObject));
        assertTrue(testManager.contains(secondObject));
        assertFalse(testManager.contains(thirdObject));

        testManager.removeFirst();
        assertFalse(testManager.contains(firstObject));
        assertFalse(testManager.contains(secondObject));
        assertFalse(testManager.contains(thirdObject));
    }

    @Test
    void size() {
        PhysicsObjectManager testManager = new PhysicsObjectManager();
        PhysicsObject firstObject = TestStubLoader.getPhysicsObject();
        PhysicsObject secondObject = TestStubLoader.getPhysicsObject();
        PhysicsObject thirdObject = TestStubLoader.getPhysicsObject();
        PhysicsObject fourthObject = TestStubLoader.getPhysicsObject();
        PhysicsObject fifthObject = TestStubLoader.getPhysicsObject();

        assertEquals(0, testManager.size());

        testManager.addFirst(firstObject);
        assertEquals(1, testManager.size());

        testManager.addLast(secondObject);
        assertEquals(2, testManager.size());

        testManager.removeFirst();
        assertEquals(1, testManager.size());

        testManager.addFirst(thirdObject);
        testManager.addLast(fourthObject);
        testManager.addLast(fifthObject);
        assertEquals(4, testManager.size());

        testManager.remove(1);
        assertEquals(3, testManager.size());

        PhysicsObjectManager.remove(fifthObject);
        assertEquals(2, testManager.size());

        testManager.clear();
        assertEquals(0, testManager.size());
    }

    @Test
    void reduceSize() {
        PhysicsObjectManager testManager = new PhysicsObjectManager();
        PhysicsObject firstObject = TestStubLoader.getPhysicsObject();
        PhysicsObject secondObject = TestStubLoader.getPhysicsObject();
        PhysicsObject thirdObject = TestStubLoader.getPhysicsObject();
        PhysicsObject fourthObject = TestStubLoader.getPhysicsObject();

        testManager.addFirst(firstObject);
        testManager.addFirst(secondObject);
        testManager.addFirst(thirdObject);
        testManager.reduceSize();
        assertEquals(2, testManager.size());

        testManager.addFirst(fourthObject);
        testManager.reduceSize();
        assertEquals(2, testManager.size());

        testManager.reduceSize();
        testManager.reduceSize();
        assertEquals(0, testManager.size());
    }

    @Test
    void increaseSize() {
        PhysicsObjectManager testManager = new PhysicsObjectManager();
        PhysicsObject firstObject = TestStubLoader.getPhysicsObject();
        PhysicsObject secondObject = TestStubLoader.getPhysicsObject();
        PhysicsObject thirdObject = TestStubLoader.getPhysicsObject();
        PhysicsObject fourthObject = TestStubLoader.getPhysicsObject();

        testManager.addFirst(firstObject);
        testManager.addFirst(secondObject);
        testManager.addFirst(thirdObject);
        testManager.increaseSize();
        assertEquals(4, testManager.size());

        testManager.addFirst(fourthObject);
        testManager.increaseSize();
        assertEquals(6, testManager.size());
    }
}